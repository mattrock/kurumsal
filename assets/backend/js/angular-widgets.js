var app = angular.module('widgetApp', []);

/*------------------------hava durumu----------------------------*/
var URL="http://api.openweathermap.org/data/2.5/weather?q=%C4%B0stanbul,tr&lang=tr&";
var KEY="appid=354dc903e5cb6ae86450ba8490a2c3c6";
URL+=KEY;

app.controller('weatherCtrl', function($scope, $http) {
   $http.get(URL).then(function (response) {
     $scope.weather = response.data.weather;
     $scope.main=response.data;
     $scope.temp=response.data.main;
   });
  
});

/*------------------------döviz----------------------------*/
URL_dolar="https://api.fixer.io/latest?base=USD&symbols=TRY";
URL_euro="https://api.fixer.io/latest?symbols=TRY";
app.controller('dovizCtrl', function($scope, $http) {
   $http.get(URL_dolar).then(function (response) {
     $scope.dolar= response.data.rates;    
   });
   $http.get(URL_euro).then(function (response) {
     $scope.euro = response.data.rates;    
   });  
});
