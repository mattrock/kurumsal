/*================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================

NOTE:
------
PLACE HERE YOUR OWN JS CODES AND IF NEEDED.
WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR CUSTOM SCRIPT IT'S BETTER LIKE THIS. */

function yukseklik(){
    var yukseklik=$(window).innerHeight();
    var head=$(header).height();
    yukseklik=yukseklik-head-70;
    yukseklik= yukseklik + "px";
    $('.min-height').css('min-height', yukseklik);
}   

$(document).ready(function(){    
             

    yukseklik();

    $(window).resize(function(){
        yukseklik();
    });
    
});
