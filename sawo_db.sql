-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 17 Oca 2018, 19:10:29
-- Sunucu sürümü: 10.1.28-MariaDB
-- PHP Sürümü: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `sawo_db`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `kuladi` varchar(255) NOT NULL,
  `sifre` varchar(255) NOT NULL,
  `adsoyad` varchar(255) NOT NULL,
  `resim` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `admin`
--

INSERT INTO `admin` (`id`, `kuladi`, `sifre`, `adsoyad`, `resim`) VALUES
(1, 'yonetici', '7fcf536f5307ff2bd9b184c37165a3c9addf8bfc', 'Kodifix', 'd7545bd041a9cc5dfe3257cba9579cee.png');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `blog_baslik` varchar(255) NOT NULL,
  `blog_icerik` longtext NOT NULL,
  `blog_etiket` text NOT NULL,
  `blog_seflink` text NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `tarih` varchar(255) NOT NULL,
  `resim` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `blog`
--

INSERT INTO `blog` (`id`, `blog_baslik`, `blog_icerik`, `blog_etiket`, `blog_seflink`, `kategori`, `tarih`, `resim`) VALUES
(35, 'Yazı 1', '&lt;p&gt;Yazı içeriği 1&lt;/p&gt;', 'yazı,blog yazısı,yazı1', 'yazi-1', '10', '14:01:2018', 'f0d8321a266707416a689194c3fc494a.png'),
(36, 'Yazı 2', '&lt;p&gt;Yazı içeriği 2&lt;/p&gt;', 'Blog yazısı,yazı,blog', 'yazi-2', '11', '14:01:2018', '76b607606f42d78af742810d94b40787.png'),
(37, 'Yazı 3', '<p>Yazı içeriği 3</p>', 'Blog yazısı,yazı,blog', 'yazi-3', '12', '14:01:2018', '90fe3934114bb6610283a1bf9a787720.png');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `blogkategori`
--

CREATE TABLE `blogkategori` (
  `id` int(11) NOT NULL,
  `kategori_adi` varchar(255) NOT NULL,
  `seflink` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `blogkategori`
--

INSERT INTO `blogkategori` (`id`, `kategori_adi`, `seflink`) VALUES
(10, 'Blog Kategori 1', 'blog-kategori-1'),
(11, 'Blog Kategori 2', 'blog-kategori-2'),
(12, 'Blog Kategori 3', 'blog-kategori-3'),
(13, 'Blog Kategori 4', 'blog-kategori-4'),
(14, 'Blog Kategori 5', 'blog-kategori-5');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `haberbulteni`
--

CREATE TABLE `haberbulteni` (
  `id` int(11) NOT NULL,
  `mail_adresi` varchar(255) NOT NULL,
  `tarih` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `haberbulteni`
--

INSERT INTO `haberbulteni` (`id`, `mail_adresi`, `tarih`) VALUES
(4, 'root@feaxer.com', '17:01:2018'),
(5, 'emrecelik1563@hotmail.com', '17.01.2018'),
(6, 'mattrock6@gmail.com', '17.01.2018');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `hakkimizda`
--

CREATE TABLE `hakkimizda` (
  `id` int(11) NOT NULL,
  `icerik` text NOT NULL,
  `baslik` varchar(255) NOT NULL,
  `seflink` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `hakkimizda`
--

INSERT INTO `hakkimizda` (`id`, `icerik`, `baslik`, `seflink`) VALUES
(1, '&lt;h2&gt;Lorem Ipsum Nedir?&lt;/h2&gt;&lt;p&gt;&lt;strong&gt;Lorem Ipsum&lt;/strong&gt;, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500&amp;#39;lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960&amp;#39;larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur.&lt;/p&gt;&lt;h2&gt;Nereden Gelir?&lt;/h2&gt;&lt;p&gt;Yaygın inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz. Kökleri M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 yıllık bir geçmişi vardır. Virginia&amp;#39;daki Hampden-Sydney College&amp;#39;dan Latince profesörü Richard McClintock, bir Lorem Ipsum pasajında geçen ve anlaşılması en güç sözcüklerden biri olan &amp;#39;consectetur&amp;#39; sözcüğünün klasik edebiyattaki örneklerini incelediğinde kesin bir kaynağa ulaşmıştır. Lorm Ipsum, Çiçero tarafından M.Ö. 45 tarihinde kaleme alınan &quot;de Finibus Bonorum et Malorum&quot; (İyi ve Kötünün Uç Sınırları) eserinin 1.10.32 ve 1.10.33 sayılı bölümlerinden gelmektedir. Bu kitap, ahlak kuramı üzerine bir tezdir ve Rönesans döneminde çok popüler olmuştur. Lorem Ipsum pasajının ilk satırı olan &quot;Lorem ipsum dolor sit amet&quot; 1.10.32 sayılı bölümdeki bir satırdan gelmektedir.&lt;/p&gt;', 'Hakkımızda', 'hakkimizda');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `iletisim`
--

CREATE TABLE `iletisim` (
  `id` int(11) NOT NULL,
  `fb_adresi` varchar(255) NOT NULL,
  `tw_adresi` varchar(255) NOT NULL,
  `g_adresi` varchar(255) NOT NULL,
  `adres` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `g_maps` text NOT NULL,
  `baslik` varchar(255) NOT NULL,
  `seflink` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `iletisim`
--

INSERT INTO `iletisim` (`id`, `fb_adresi`, `tw_adresi`, `g_adresi`, `adres`, `mail`, `tel`, `g_maps`, `baslik`, `seflink`) VALUES
(1, 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.google.com.tr/', 'Ağaoğlu My Office 212 Taşocağı Cad. Kat: 15 Daire:244, 34218 Güneşli, Bağcılar/İstanbul', 'info@kodifix.com', ' (0212) 532 12 18', '&amp;lt;iframe src=&quot;https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12036.045195465178!2d28.810276!3d41.046882!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0:0x982aa0dc7cd880a!2sKodifix+Bilişim+Hizmetleri!5e0!3m2!1str!2str!4v1515166233142&quot; width=&quot;600&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0&quot; allowfullscreen&amp;gt;&amp;lt;/iframe&amp;gt;', 'İletişim', 'iletisim');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kategoriler`
--

CREATE TABLE `kategoriler` (
  `id` int(11) NOT NULL,
  `kategori_adi` varchar(255) NOT NULL,
  `seflink` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `kategoriler`
--

INSERT INTO `kategoriler` (`id`, `kategori_adi`, `seflink`) VALUES
(14, 'Ürün Kategori 1', 'urun-kategori-1'),
(15, 'Ürün Kategori 2', 'urun-kategori-2'),
(16, 'Ürün Kategori 3', 'urun-kategori-3'),
(17, 'Ürün Kategori 4', 'urun-kategori-4'),
(18, 'Ürün Kategori 5', 'urun-kategori-5'),
(20, 'Ürün Kategori 6', 'urun-kategori-6'),
(21, 'Ürün kategori 7', 'urun-kategori-7'),
(22, 'Ürün Kategori 8', 'urun-kategori-8');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `menuayarlari`
--

CREATE TABLE `menuayarlari` (
  `id` int(11) NOT NULL,
  `menu_adi` varchar(255) NOT NULL,
  `menu_eklenme_tarihi` varchar(255) NOT NULL,
  `menu_alt_kategori_id` varchar(255) NOT NULL,
  `seflink` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `menuayarlari`
--

INSERT INTO `menuayarlari` (`id`, `menu_adi`, `menu_eklenme_tarihi`, `menu_alt_kategori_id`, `seflink`) VALUES
(101, 'Ana Kategori', '17.01.2018', '14,15,16,17,18,20,21', 'ana-kategori'),
(102, 'Ana Kategori 3', '17.01.2018', '16,17,18', 'ana-kategori-3');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `referanslar`
--

CREATE TABLE `referanslar` (
  `id` int(11) NOT NULL,
  `referans_adi` varchar(255) NOT NULL,
  `referans_resmi` text NOT NULL,
  `referans_tarihi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `referanslar`
--

INSERT INTO `referanslar` (`id`, `referans_adi`, `referans_resmi`, `referans_tarihi`) VALUES
(15, 'Referans 1', 'c9c12e51a590a40bed31d69138bb1aa8.png', '17.01.2018'),
(16, 'Referans 2', 'c7f58d5bc46e2b12e1b59ef9d97db0eb.png', '17.01.2018'),
(18, 'Referans 3', '0cf769711de6e05a973b0fb8cc8436fc.png', '17.01.2018');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `siteayarlari`
--

CREATE TABLE `siteayarlari` (
  `id` int(11) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `site_favicon` varchar(255) NOT NULL,
  `site_adi` varchar(255) NOT NULL,
  `site_aciklamasi` text NOT NULL,
  `site_anahtar` varchar(255) NOT NULL,
  `yazar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `siteayarlari`
--

INSERT INTO `siteayarlari` (`id`, `site_logo`, `site_favicon`, `site_adi`, `site_aciklamasi`, `site_anahtar`, `yazar`) VALUES
(1, '53411861515943966.png', '87692261515943979.png', 'Kodifix Bilişim Hizmetleri', 'Kodifix bilişim Hizmetleri Kurumsal Panel Version 1.1', 'kelime1,kelime2,kelime3', 'Kodifix');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sliderlar`
--

CREATE TABLE `sliderlar` (
  `id` int(11) NOT NULL,
  `slider_adi` varchar(255) NOT NULL,
  `slider_resmi` varchar(255) NOT NULL,
  `slider_tarih` varchar(255) NOT NULL,
  `slider_slogan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `sliderlar`
--

INSERT INTO `sliderlar` (`id`, `slider_adi`, `slider_resmi`, `slider_tarih`, `slider_slogan`) VALUES
(44, 'Slider 1', 'ff00db21429150fd51258076fb7e217b.png', '14.01.2018', 'Slider slogan 1'),
(45, 'Slider 2', '3423534a21d1b5d16700b050814fb24b.png', '14.01.2018', 'Slider slogan 2'),
(46, 'Slider 3', '7b8a2df9b1b847d1ce79559e6822da0b.png', '14.01.2018', 'Slider slogan 3');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunler`
--

CREATE TABLE `urunler` (
  `id` int(11) NOT NULL,
  `urun_adi` varchar(255) NOT NULL,
  `urun_tarih` varchar(255) NOT NULL,
  `urun_kategori` varchar(255) NOT NULL,
  `seflink` text NOT NULL,
  `urun_aciklamasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `urunler`
--

INSERT INTO `urunler` (`id`, `urun_adi`, `urun_tarih`, `urun_kategori`, `seflink`, `urun_aciklamasi`) VALUES
(144, 'Ürün 1', '14.01.2018', '14', 'urun-1', 'Ürün açıklaması 1'),
(145, 'Ürün 2', '14.01.2018', '15', 'urun-2', 'Ürün açıklaması 2'),
(146, 'Ürün 3', '17.01.2018', '16', 'urun-3', 'Ürün açıklaması 3');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunresimleri`
--

CREATE TABLE `urunresimleri` (
  `id` int(11) NOT NULL,
  `urunresmi` text NOT NULL,
  `urun_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `urunresimleri`
--

INSERT INTO `urunresimleri` (`id`, `urunresmi`, `urun_id`) VALUES
(97, '5ea9eb3d3137906c4a5207e9f42b5b82.png', 133),
(98, 'e6d6bfb557156eed3f895c86c29a064d.png', 134),
(99, '42ceeaa9d6beb37eb6853b3023ecf53b.png', 135),
(102, 'bba3fab396edb0c1576ed5ae49c187b1.png', 138),
(103, 'bde8b3c4e561511d11b13ba519eb5c28.png', 139),
(104, '8e01402be83291f4f50a298b61f8a4d5.png', 140),
(105, 'b2cde95c9b1a2da0d6ae4a842030069e.png', 141),
(106, 'ce115e41e06d3d10dad4ba7e73897b90.png', 142),
(107, 'a0f0903271b72a7e5ee9625d11b7844e.gif', 143),
(108, 'd0083ea787224c9790de788d9c15c7af.png', 144),
(109, '70c4a48e4f71b7c85faa30032c0feb06.png', 145),
(110, '9c0db03abde52920fbf03294dff2bd97.png', 146),
(111, '9c4e939a95d3ce1f022da18d1a6b955c.jpg', 147);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `blogkategori`
--
ALTER TABLE `blogkategori`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `haberbulteni`
--
ALTER TABLE `haberbulteni`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `hakkimizda`
--
ALTER TABLE `hakkimizda`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `iletisim`
--
ALTER TABLE `iletisim`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `kategoriler`
--
ALTER TABLE `kategoriler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `menuayarlari`
--
ALTER TABLE `menuayarlari`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `referanslar`
--
ALTER TABLE `referanslar`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `siteayarlari`
--
ALTER TABLE `siteayarlari`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `sliderlar`
--
ALTER TABLE `sliderlar`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `urunler`
--
ALTER TABLE `urunler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `urunresimleri`
--
ALTER TABLE `urunresimleri`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Tablo için AUTO_INCREMENT değeri `blogkategori`
--
ALTER TABLE `blogkategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Tablo için AUTO_INCREMENT değeri `haberbulteni`
--
ALTER TABLE `haberbulteni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Tablo için AUTO_INCREMENT değeri `hakkimizda`
--
ALTER TABLE `hakkimizda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `iletisim`
--
ALTER TABLE `iletisim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `kategoriler`
--
ALTER TABLE `kategoriler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Tablo için AUTO_INCREMENT değeri `menuayarlari`
--
ALTER TABLE `menuayarlari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- Tablo için AUTO_INCREMENT değeri `referanslar`
--
ALTER TABLE `referanslar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Tablo için AUTO_INCREMENT değeri `siteayarlari`
--
ALTER TABLE `siteayarlari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `sliderlar`
--
ALTER TABLE `sliderlar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- Tablo için AUTO_INCREMENT değeri `urunler`
--
ALTER TABLE `urunler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- Tablo için AUTO_INCREMENT değeri `urunresimleri`
--
ALTER TABLE `urunresimleri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
