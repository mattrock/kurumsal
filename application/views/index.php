<?php  $this->load->view("includes/header"); ?>
        <div id="masterslider" class="master-slider ms-skin-default mb-0">
          
		  <?php foreach($sliderlist as $yaz) { ?>
            <div class="ms-slide">
                
                <img src="<?php echo base_url("uploads/") . $yaz->slider_resmi; ?>" data-src="<?php echo base_url("uploads/") . $yaz->slider_resmi; ?>" alt="<?php echo base_url("uploads/") . $yaz->slider_adi; ?> "/>  

               

                <h2 class="ms-layer pi-caption01" 
                    style="left: 0; top: 340px;" 
                    data-type="text" 
                    data-effect="left(short)" 
                    data-duration="300"
                    data-hide-effect="fade" 
                    data-delay="300" 
                    >
                    <?php $slideradsub = $yaz->slider_adi; echo substr($slideradsub,0,25); ?>
                </h2>

                <h6 class="ms-layer pi-caption01 slogan" 
                    style="left: 0; top: 400px;" 
                    data-type="text"
                    data-effect="left(short)" 
                    data-duration="300"
                    data-hide-effect="fade" 
                    data-delay="600" 
                    >
              <?php $slideradsubs = $yaz->slider_slogan; echo substr($slideradsubs,0,200); ?>
                </h6>

                
            </div>
		  <?php } ?>
            

        </div>

        <div class="page-content parallax parallax01 mb-70">
            <div class="container">
                <div class="row services-negative-top">
				
				<?php foreach($blogyazi as $yaz) {  ?>
                    <div class="col-md-4 col-sm-4">
                        <div class="service-feature-box">
                            <div class="service-media">
                                <img src="<?php echo base_url("uploads/") . $yaz->resim; ?>" alt="<?php echo $yaz->blog_baslik; ?>"/>

                                <a href="<?php echo base_url("hizmet/") . $yaz->blog_seflink; ?>" class="read-more02">
                                    <span>
                                        Devamı
                                        <i class="fa fa-chevron-right"></i>
                                    </span>
                                </a>
                            </div><!-- .service-media end -->

                            <div class="service-body">
                                <div class="custom-heading">
                                    <h4><?php echo $yaz->blog_baslik; ?></h4>
                                </div><!-- .custom-heading end -->

                                <p>
                  <?php $yaz =  strip_tags(htmlspecialchars_decode($yaz->blog_icerik)); echo mb_substr($yaz,0,350); ?>
                                </p>
                            </div><!-- .service-body end -->
                        </div><!-- .service-feature-box-end -->
                    </div><!-- .col-md-4 end -->
					<?php } ?>
					

                <div class="row" >
                    <div class="col-md-12" style="margin-top:30px;">
                        <a href="<?php echo base_url("/hizmetler"); ?>" class="btn btn-big btn-yellow btn-centered">
                            <span>
                                DETAYLARI GÖSTER
                            </span>
                        </a>
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-heading02">
                            <h2>Hizmetlerimiz</h2>
                            <p>
                               
ÖNEMLİ TAŞIMACILIK HİZMETLERİ
                            </p>
                        </div><!-- .custom-heading02 end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->

                <div class="row mb-30">
                    <div class="col-md-6 col-sm-6">
                        <div class="service-icon-left-boxed">
                            <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                <img src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-checklist-2.svg" alt=""/>
                            </div><!-- .icon-container end -->

                            <div class="service-details">
                                <h3>Lojistikte Sözleşme</h3>

                                <p>
                                    

Özel nakliye hizmetine mi ihtiyacınız var?  Lojistikte Sözleşme ile hızlı, güvenli, doğru gönderim ve müşteri memnuniyeti sağlandı.
                                </p>
                            </div><!-- .service-details end -->
                        </div><!-- .service-icon-left-boxed end -->
                    </div><!-- .col-md-6 end -->

                    <div class="col-md-6 col-sm-6">
                        <div class="service-icon-left-boxed">
                            <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                <img src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-truck-8.svg" alt=""/>
                            </div><!-- .icon-container end -->

                            <div class="service-details">
                                <h3>   Kara Taşımacılığı</h3>

                                <p>
                                

Nakliye şirketi, tüm tedarik zinciri ağlarını kullanarak en iyi lojistik hizmetlerini sunmaktadır. Mallarınızın nakli için nakliye çözümlerimizi kullanın.
                                </p>
                            </div><!-- .service-details end -->
                        </div><!-- .service-icon-left-boxed end -->
                    </div><!-- .col-md-6 end -->
                </div><!-- .row.mb-30 end -->

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="service-icon-left-boxed">
                            <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                <img src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-forklift-truck-5.svg" alt=""/>
                            </div><!-- .icon-container end -->

                            <div class="service-details">
                                <h3>Depolama</h3>

                                <p>
                                    
Nakliye, akıllı depo çözümü sunar. Modern ve akıllı depolarımızı kullanarak hizmetlerinizi gerçekleştirmekteyiz. 
                                </p>
                            </div><!-- .service-details end -->
                        </div><!-- .service-icon-left-boxed end -->
                    </div><!-- .col-md-6 end -->

                    <div class="col-md-6 col-sm-6">
                        <div class="service-icon-left-boxed">
                            <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                                <img src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-touch-desktop.svg" alt=""/>
                            </div><!-- .icon-container end -->

                            <div class="service-details">
                                <h3>Danışmanlık Servisleri</h3>

                                <p>
                                    

Taşıma araçlarının sizin için doğru olacağını bilmiyor musunuz yoksa tam tedarik zinciri yönetimi için birine mi ihtiyacınız var? Lütfen bizimle iletişime geçin. Profesyonel ekibimiz size yardımcı olmaktan mutluluk duyacaktır.
                                </p>
                            </div><!-- .service-details end -->
                        </div><!-- .service-icon-left-boxed end -->
                    </div><!-- .col-md-6 end -->
                </div><!-- .row.mb-30 end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->

        <div class="page-content custom-bkg bkg-dark-blue column-img-bkg dark mb-70">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2 custom-col-padding-both">
                        <div class="custom-heading">
                            <h3>ÇALIŞMA ALANLARI</h3>
                        </div><!-- .custom-heading end -->

                        <p>
                          
                            Evden eve nakliyat, parça eşya taşıma, ofis taşımacılığı, eşya depolama olanlarımız mevcuttur. Detaylar için listeyi kontrol edin.
                        </p>

                        <ul class="service-list clearfix">
                            <li>
                                <div class="icon-container">
                                    <img class="svg-white" src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-cargo-box-2.svg" alt="icon"/>                                    
                                </div><!-- .icon-container end -->

                                <p>
                                    
Tüketici Paketli Ürünler
                                </p>
                            </li>

                            <li>
                                <div class="icon-container">
                                    <img class="svg-white" src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-forklift-truck-4.svg" alt="icon"/>                                    
                                </div><!-- .icon-container end -->

                                <p>
                                    Eşya Depolama
                                </p>
                            </li>

                            <li>
                                <div class="icon-container">
                                    <img class="svg-white" src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-truck-8.svg" alt="icon"/>                                    
                                </div><!-- .icon-container end -->

                                <p>
                                    Evden Eve Nakliyat
                                </p>
                            </li>

                            <li>
                                <div class="icon-container">
                                    <img class="svg-white" src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-cargo-retail.svg" alt="icon"/>                                    
                                </div><!-- .icon-container end -->

                                <p>
                                    Parça Eşya Taşıma
                                </p>
                            </li>

                            <li>
                                <div class="icon-container">
                                    <img class="svg-white" src="<?php echo base_url("assets/frontend/"); ?>img/svg/pi-forklift-truck-5.svg" alt="icon"/>                                    
                                </div><!-- .icon-container end -->

                                <p>
                                    Ofis Taşımacılığı
                                </p>
                            </li>
                        </ul><!-- .service-list end -->
                    </div><!-- .col-md-6 end -->

                    <div class="col-md-6 img-bkg01">
                        <div>&nbsp;</div>
                    </div>
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content.bkg-dark-blue end -->
<?php  $this->load->view("includes/footer"); ?>