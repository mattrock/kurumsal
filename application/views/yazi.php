
        <div class="page-title-style01 page-title-negative-top pt-bkg04">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Karayolu Taşımacılığı</h1>

                        <div class="breadcrumb-container">
                            <ul class="breadcrumb clearfix">
                                <li>Buradasınız:</li>
                                <li>
                                    <a href="<?php echo base_url("/hizmetler"); ?>">Hizmetler</a>
                                </li>
                                <li>
                                    <a href="<?php echo $yazilar->blog_baslik; ?>"><?php echo $yazilar->blog_baslik; ?></a>
                                </li>
                            </ul><!-- .breadcrumb end -->
                        </div><!-- .breadcrumb-container end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-title-style01.page-title-negative-top end -->


        <div class="page-content">
            <div class="container">
                <div class="row">
                   

                    <div class="col-md-12">
                        <img src="img/pics/img27.jpg" alt=""/>

                        <br />

                        <div class="custom-heading">
                            <h2><?php echo $yazilar->blog_baslik; ?></h2>
                        </div>

                        <p>
                           Türkiye'de eşya veya yolcu taşıma hizmetleri, genellikle karayoluyla sağlanmaktadır. 2003 yılı itibariyle yurt içi eşya taşımacılığının %92'si, yolcu taşımacılığının ise %96'sı karayoluyla gerçekleştirilmiştir. Buna karşılık, uluslararası eşya taşımalarında bu oranın, ihraç taşımalarında %16,3 (Avrupa'ya %77,6), ithal taşımalarında ise %6,4 (Avrupa'dan %76,3)'lerde kaldığı görülmektedir. Aynı dönemde Türkiye'de yolcu taşıma işiyle iştigal eden işletmelerin sayısı 567 (bunların 155'i uluslararası yolcu taşımacılığıyla uğraşmakta olup, 79 adedi uluslararası düzenli hatta sahiptir), uluslararası eşya taşımacılığını meslek edinenler ise 996 adettir. Karayollarımızda toplam koltuk sayısı 402.506 olan 9.527 adet otobüs (bunların toplam 73.852 adet koltuk kapasiteli 1521'i uluslararası çalışmaktadır) ve toplam eşya taşıma kapasitesi 1.314.637 ton olan 30.265 çekici, 34.245 yarı römork, 2.268 römork, 12.163 tanker ve 13.412 kamyon dolaşmaktadır.

Bütün bu veriler, Türk ekonomisi bakımından karayolu taşımacılığının ne kadar önemli olduğunu ortaya koymaktadır. Bununla birlikte, Türkiye, 19.07.2003 tarihine kadar karayolu taşımacılığının bilhassa idarî boyutunu düzenleyen genel bir kanunu Meclisten çıkartamamıştır. Taşıma hizmetleri hakkındaki düzenlemelerin çoğunluğu yönetmelik ve genelge düzeyinde kalmıştır. Mevzuatımızdaki bu yetersizliği, Türkiye'nin karayolu taşımacılığı hakkında milli bir politikaya sahip olmayışına dayandırmak mümkündür. Taşımacılık alanında rol oynayan çeşitli menfaat gruplarının, bir araya gelerek müşterek menfaatlerini yansıtacak bir kanun arayışına girmemeleri de, eksikliğin bir diğer sebebi olarak gösterilebilir. 

Ulaştırma Bakanlığının çabaları sonucu, nihayet 10.07.2003 tarihinde 4925 sayılı Karayolu Taşıma Kanunu, TBMM.de kabul edilmiş ve 19.07.2003 tarihinde de 25173 sayılı Resmi Gazetede ilân edilerek yürürlüğe sokulmuştur. Kanun, "Başlangıç Hükümleri", "Uluslararası Taşımalar", "Sorumluluk ve Sigorta" ve "Çeşitli Hükümler" başlıklı 4 kısımdan ve 38 maddeden ibarettir.
Ulaştırma Bakanlığınca Kanunun 34. maddesine dayanılarak hazırlanan Karayolu Taşıma Yönetmeliği ise, 25.02.2004 tarihinde 25384 sayılı Resmi Gazetede yayımlanarak yürürlüğe girmiştir. Toplam 90 maddeden oluşan bu Yönetmelikte Kanunun 34. maddesinde sayılan hususlar hakkında düzenlemeye gidilmiştir.


                        </p>

                        <p>
                        </p>

                        <br />

                        <img class="float-right" width="360" src="img/pics/img25.jpg" alt=""/>

                        İşbu makale, yıllar sonra mevzuatımızdaki önemli bir eksikliği doldurmak maksadıyla çıkartılan Karayolu Taşıma Kanununu ve Yönetmeliğini değerlendirmek için kaleme alınmıştır. Makalede Kanun ve Yönetmeliğin hukukumuza hangi katkıları sağladığına, beklentileri ne ölçüde karşıladığına, varsa zafiyetlerine işaret olunacak ve en önemlisi Kanunun 1. maddesinde ifade olunan amaçlarla ve Avrupa Birliğini kuran Roma Antlaşmasının 74. maddesi çerçevesinde oluşturulan ortak taşıma politikaları ile bağdaşıp bağdaşmadığı sorusuna cevap aranacaktır. Bu maksatla Kanun ve Yönetmeliğin sadece önemli maddeleri, Kanunun sistematiği içinde ele alınmaya çalışılacaktır.

                        <br />
                        <br />
<br />
                        <br />
                        

                        
                    </div><!-- .col-md-9 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div>    
