
        <div class="page-title-style01 page-title-negative-top pt-bkg08">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Bize Ulaşın</h1>

                        <div class="breadcrumb-container">
                            <ul class="breadcrumb clearfix">
                                
                            </ul><!-- .breadcrumb end -->
                        </div><!-- .breadcrumb-container end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-title-style01.page-title-negative-top end -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="custom-heading">
                            <h3>İletİşİm Formu</h3>
                        </div><!-- .custom-heading.left end -->

                        <p>
                            
                        </p>

                        <br />

                        <!-- .contact form start -->
                        <form class="wpcf7 clearfix">
                            <fieldset>
                                <label>
                                    <span class="required">*</span> İsteğiniz:
                                </label>

                                <select class="wpcf7-form-control-wrap wpcf7-select" id="contact-inquiry">
                                    <option value="I need an offer for contract logistics">Sözleşmeli lojistik için bir teklif istiyorum.</option>
                                 
                                    <option value="I want to become your partner">Sizinle çalışmak istiyorum.</option>
                                    <option value="I have some other request">Başka bir isteğim var.</option>
                                </select>
                            </fieldset>

                            <fieldset>
                                <label>
                                    <span class="required">*</span> Ad:
                                </label>

                                <input type="text" class="wpcf7-text" id="contact-name">
                            </fieldset>

                            <fieldset>
                                <label>
                                    <span class="required">*</span> Soyad:
                                </label>

                                <input type="text" class="wpcf7-text" id="contact-last-name">
                            </fieldset>

                            <fieldset>
                                <label>
                                    <span class="required">*</span> E-mail:
                                </label>

                                <input type="url" class="wpcf7-text" id="contact-email">
                            </fieldset>

                            <fieldset>
                                <label>
                                    <span class="required">*</span> Mesaj:
                                </label>

                                <textarea rows="8" class="wpcf7-textarea" id="contact-message"></textarea>
                            </fieldset>

                            <input type="submit" class="wpcf7-submit" value="Gönder" />
                        </form><!-- .wpcf7 end -->
                    </div><!-- .col-md-6 end -->

                    <div class="col-md-6">
                        <div class="custom-heading">
                            <h3>Konum</h3>
                        </div><!-- .custom-heading end -->

                        <div id="map"><?php echo htmlspecialchars_decode($iletisimcek->g_maps); ?></div>

                        <div class="custom-heading">
                            <h4>
ŞİRKET BİLGİSİ</h4>
                        </div><!-- .custom-heading end -->

                        <address>
                            <?php echo $iletisimcek->adres; ?>
                        </address>

                        <span class="text-big colored">
                            <?php echo $iletisimcek->tel; ?>
                        </span>
                        <br />

                        <a href="mailto:<?php echo $iletisimcek->tel; ?>"><?php echo $iletisimcek->mail; ?></a>
                    </div><!-- .col-md-6 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->
