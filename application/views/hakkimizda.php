
        <div class="page-title-style01 page-title-negative-top pt-bkg01">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Hakkımızda</h1>

                        <div class="breadcrumb-container">
                            <ul class="breadcrumb clearfix">
                               
                            </ul><!-- .breadcrumb end -->
                        </div><!-- .breadcrumb-container end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-title-style01.page-title-negative-top end -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-heading02">
                            <h2>Biz Kimiz?</h2>
                            <p>
                                Sadece bİr NAKLİYECİ olmaktan öte
                            </p>
                        </div><!-- .custom-heading02 end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="intro-title">
                            
                          <?php echo htmlspecialchars_decode($hakkimizdacek->icerik); ?>
                        </div><!-- .intro-title end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->

        

        

        

        <div class="page-content">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-heading02 simple">
                            <h2>Referanslarımız</h2>
                        </div><!-- .custom-heading02 end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="carousel-container">
                            <div id="client-carousel" class="owl-carousel owl-carousel-navigation">
                                <div class="owl-item"><img src="<?php echo base_url('assets/frontend/'); ?>img/pics/client01.png" alt=""/></div>
                                <div class="owl-item"><img src="<?php echo base_url('assets/frontend/'); ?>img/pics/client02.png" alt=""/></div>
                                <div class="owl-item"><img src="<?php echo base_url('assets/frontend/'); ?>img/pics/client03.png" alt=""/></div>
                                <div class="owl-item"><img src="<?php echo base_url('assets/frontend/'); ?>img/pics/client04.png" alt=""/></div>
                                <div class="owl-item"><img src="<?php echo base_url('assets/frontend/'); ?>img/pics/client05.png" alt=""/></div>
                                <div class="owl-item"><img src="<?php echo base_url('assets/frontend/'); ?>img/pics/client06.png" alt=""/></div>
                            </div><!-- .owl-carousel.owl-carousel-navigation end -->
                        </div><!-- .carousel-container end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->