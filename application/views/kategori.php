
        <div class="page-title-style01 page-title-negative-top pt-bkg03">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1> Hizmetler</h1>

                        <div class="breadcrumb-container">
                            <ul class="breadcrumb clearfix">
                                <li>Buradasınız:</li>
                                <li>
                                    <a href="<?php echo base_url("/hizmetler"); ?>">Hizmetler</a>
                                </li>
                               
                            </ul><!-- .breadcrumb end -->
                        </div><!-- .breadcrumb-container end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-title-style01.page-title-negative-top end -->


        <div class="page-content custom-bkg bkg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-heading02">
                            <h2>Uzmanlaşmış Taşımacılık Hizmetleri</h2>
                            <p>
ÖNEMLİ NAKLİYECİLİK HİZMETLERİ</p>
                        </div><!-- .custom-heading02 end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->

                
				
				<?php foreach($veriler as $yaz) { ?>
                    <div class="col-md-4 col-sm-4" style="margin-bottom:25px;">
					
                        <div class="service-feature-box">
                            <div class="service-media">
                                <img src="<?php echo base_url("uploads/") . $yaz->resim; ?>" alt="<?php echo $yaz->blog_baslik; ?>"/>

                                <a href="<?php echo base_url("hizmet/") . $yaz->blog_seflink; ?>" class="read-more02">
                                    <span>
                                        Devamı
                                        <i class="fa fa-chevron-right"></i>
                                    </span>
                                </a>
                            </div><!-- .service-media end -->

                            <div class="service-body">
                                <div class="custom-heading">
                                    <h4> <?php echo $yaz->blog_baslik; ?></h4>
                                </div><!-- .custom-heading end -->

                                <p>
                                 <?php echo mb_substr(htmlspecialchars_decode($yaz->blog_icerik),0,360) . "..."; ?>
                                </p>
                            </div><!-- .service-body end -->
                        </div><!-- .service-feature-box-end -->
                    </div><!-- .col-md-4 end -->
					
                    <?php } ?>
                    


                
            
        </div><!-- .page-content end -->  

<div class="row">
<div class="container">
<ul class="pagination">
<?php echo $linkler; ?>
</ul>
</div>		
</div>		

        <div class="page-content parallax parallax02 mb-70 dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-heading02 simple">
                            <h2> Şirket Sözü</h2>
                        </div><!-- .custom-heading02 end -->

                        <div class="statement">
                            <p>
                              
Bir nakliyeci olarak, gönderilerinizin güvenli, hızlı ve zamanında yolculuk etmesini sağlamak için tüm tedarik zinciri yönetimine özen göstereceğiz.
                            </p>
                        </div>
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="carousel-container">
                            <div id="client-carousel" class="owl-carousel owl-carousel-navigation">
                                <div class="owl-item"><img src="<?php echo base_url("uploads/"); ?>img/pics/client01.png" alt=""/></div>
                                <div class="owl-item"><img src="img/pics/client02.png" alt=""/></div>
                                <div class="owl-item"><img src="img/pics/client03.png" alt=""/></div>
                                <div class="owl-item"><img src="img/pics/client04.png" alt=""/></div>
                                <div class="owl-item"><img src="img/pics/client05.png" alt=""/></div>
                                <div class="owl-item"><img src="img/pics/client06.png" alt=""/></div>
                            </div><!-- .owl-carousel.owl-carousel-navigation end -->
                        </div><!-- .carousel-container end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->

