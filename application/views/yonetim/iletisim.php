 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                  
					<div class="col s12 m8 l9">
					
					
                 
				 <div id="basic-form" class="section">
              <div class="row">
                <div class="col s12 m12 24">
				<?php echo $this->session->userdata("basarili"); ?>
				
				
                  <div class="card-panel">
                    <h4 class="header2">İletişim Ayarları</h4>
                    <div class="row">
					
					
                    
                    
                      
                        
                   
					
					
                      <form class="col s12" action="/yonetimpaneli/iletisim" method="POST">
                        <div class="row">
						
						
                          <div class="input-field col s12">
                            <input id="name" type="text" name="baslik1" value="<?php echo $iletisim->baslik; ?>">
                            <label for="first_name" class="">Sayfa Başlığı</label>
                          </div>
                       
					 
                          <div class="input-field col s12">
                            <input id="name" type="text" name="fb" value="<?php echo $iletisim->fb_adresi; ?>">
                            <label for="first_name">Facebook Link</label>
                          </div>
						  <div class="input-field col s12">
                            <input id="name" type="text" name="tw" value="<?php echo $iletisim->tw_adresi; ?>">
                            <label for="first_name">Twitter Link</label>
                          </div>
						  <div class="input-field col s12">
                            <input id="name" type="text" name="google" value="<?php echo $iletisim->g_adresi; ?>">
                            <label for="first_name">Google Plus Link</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                            <textarea id="message" class="materialize-textarea" name="adres"><?php echo $iletisim->adres; ?></textarea>
                            <label for="message">Adres</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                            <input id="password" type="text" name="mail" value="<?php echo $iletisim->mail; ?>">
                            <label for="password">Mail</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                            <input id="password" type="text" name="tel" value="<?php echo $iletisim->tel; ?>">
                            <label for="password">Tel</label>
                          </div>
						  <div class="input-field col s12">
                            <input id="password" type="text" name="googlemaps" value="<?php echo $iletisim->g_maps; ?>">
                            <label for="password">Google Maps</label>
                          </div>
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Kaydet
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
				 
				 
                </div>
                   
                    
               
            </section>
           

        </div>
       

    </div>
    <!-- END MAIN -->

 <?php $this->load->view("yonetim/includes/footer"); ?>

    