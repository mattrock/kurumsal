 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                  
					<div class="col s12 m8 l9">
                 
				 <div id="basic-form" class="section">
              <div class="row">
                <div class="col s12 m12 24">
				<?php echo $this->session->userdata("basarili"); ?>
				<?php echo $this->session->userdata("basarisiz"); ?>
                  <div class="card-panel">
                    <h4 class="header2">Ürün Ekle</h4>
                    <div class="row">
                      <form class="col s12" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
						<div class="file-field input-field col s12">
                         <div class="resim"></div>          
						<input class="file-path validate" type="text" name="file">
						
                        <div class="btn">
                          <span>Ürün Resmi Seç</span>
                          <input type="file" class="file" name="file" required>
                        </div>
                      </div>
					  
                          <div class="input-field col s12">
                            <input id="name" type="text" name="urunadi" required>
                            <label for="first_name">Ürün Adı</label>
                          </div>
                        </div>
						<div class="row">
                        <div class="input-field col s12">
                          <textarea id="textarea1" class="materialize-textarea" name="aciklamasi"></textarea>
                          <label for="textarea1" class="">Ürün Açıklaması</label>
                        </div>
                      </div>
                        <div class="row">
<div class="input-field col s12">                        
						<select name="kategori" id="kategorim">
	
                    <option value="" disabled selected>Kategori Seç</option>
					<?php foreach($kategoriler as $yaz) { ?> 
                    <option value="<?php echo $yaz->id; ?>"><?php echo $yaz->kategori_adi; ?></option>
                    <?php } ?>
					
                  </select>
				  </div>
                        </div>
                        
                        <div class="row">
                          
						  
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Kaydet
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
				 
				 
                </div>
                   
                    
               
            </section>
           

        </div>
       

    </div>
    <!-- END MAIN -->

 <?php $this->load->view("yonetim/includes/footer"); ?>

    <script>
	
	$(document).ready(function(){
		
		var preview = $(".file");
     $(".file").change(function(event){
     var input = $(event.currentTarget);
     var file = input[0].files[0];
     var reader = new FileReader();
     reader.onload = function(e){
     image_base64 = e.target.result;
     $(".resim").html("<img src='"+image_base64+"' style='max-width:100px;'/>");
     };
     reader.readAsDataURL(file);
     });
		
		
		
		
		
		
		$( "form" ).submit(function() {
			
        var option = $(".select-dropdown").val();
		console.log(option);
		if(option == "Kategori Seç"){
			$(".select-dropdown").css("border-color","red");
			return false;
		}
		else{
			return true;
		}
		
		
        });
		
		
	
	
	    
		
	});
	
	</script>