 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                        <div class="col s12 m4" style="margin-top:30px; margin-bottom:30px;">
                          <a class="btn-floating btn-large waves-effect waves-light " href="/yonetimpaneli/yaziekle"><i class="mdi-content-add"></i></a>
                         
                        </div>
                        
                      
   
					
					
					<div class="col s12 m8 l9">
					<?php echo $this->session->userdata("basarili"); ?>
					<?php echo $this->session->userdata("silindi"); ?>
                  <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Yazı Adı</th>
                            <th>Yazı İçeriği</th>
                            <th>Tarihi</th>
                            <th>Kategori</th>
                            <th>İşlemler</th>
                            
                        </tr>
                    </thead>
                 
                
                 
                    <tbody>
					
					
					
                          <?php foreach($veriler as $yazdir) { ?>
						          <?php $kategoriesle = $this->sql->kategoriesle1($yazdir->kategori); 
								  
								  if($kategoriesle == null){
									  $kategoriesle = array(
									  "kategori_adi" => "ds",
									  );
								  }
								  
								  
								  ?>
						  <?php foreach($kategoriesle as $yaz) {  ?>
                        <tr>
						
                            <td><?php echo $yazdir->id; ?></td>
                            <td><?php $ar = html_entity_decode($yazdir->blog_baslik); echo strip_tags($ar);  ?></td>
                            <td><?php echo strip_tags(htmlspecialchars_decode(mb_substr($yazdir->blog_icerik,0,60))) . "..."; ?></td>
                            <td><?php echo $yazdir->tarih; ?></td>
                           <td><?php echo @ $son=strip_tags($yaz->kategori_adi); if($son==""){echo 'Kategori Ekleyin';} ?></td>
                            <td><a href="/yonetimpaneli/blogyaziduzenle/<?php echo $yazdir->id; ?>">Düzenle</a> / <a href="javascript:;" onclick="sil(<?php echo $yazdir->id; ?>);">Sil</a></td>
                            
                        </tr>
						  <?php } } ?>
                    </tbody>
                  </table>
                </div>
                   
               
            </section>
			<div class="row">
	<div class="col s4 m4 ">&nbsp;</div>
				
                <div class="col s4 m8 cpm">
				  
				   <?php echo $linkler; ?>
   
				 
  
  
             </div>     
<div class="col s4 m4 "></div>
</div>			 
</div>
        </div>
        

    </div>
    

 <?php $this->load->view("yonetim/includes/footer"); ?>
  <style>
 
 .cpm a{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 margin-left:2px;
	 border-radius:3px;
	 color:#222;
 }
 
 .cpm strong{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 color:#00bcd4;
 }
 
 #data-table-simple{
	 
 }
 
 
 </style>

    <script>
	
	function sil(id){
		    var txt;
    var r = confirm("Silmek istediğinize emin misiniz ?");
    if (r == true) {
        location.replace('/yonetimpaneli/blogyazisil/'+id);
    } else {
       return false;
    }
			
			
		}
	
	
	
	</script>