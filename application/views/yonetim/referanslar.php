 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                        <div class="col s12 m4" style="margin-top:30px; margin-bottom:30px;">
                          <a class="btn-floating btn-large waves-effect waves-light " href="/yonetimpaneli/referansekle"><i class="mdi-content-add"></i></a>
                         
                        </div>
                        
                      
   
					
					
					<div class="col s12 m8 l9">
					<?php echo $this->session->flashdata("basarili"); ?>
                  <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Referans id</th>
                            <th>Referans Adı</th>
                            <th>Referans Resmi</th>
                            <th>Referans Eklenme Tarihi</th>
                            <th>İşlemler</th>
                            
                        </tr>
                    </thead>
                 
                
                 
                    <tbody>
                       
					   
					   <?php foreach($veriler as $yaz) { ?>
                        <tr>
                            <td><?php echo $yaz->id; ?></td>
                            <td><?php echo $yaz->referans_adi; ?></td>
                            <td><img src="<?php echo base_url("uploads/"). $yaz->referans_resmi; ?>" style="max-width:100px; min-width:100px; border:1px solid #e1e1e1; border-radius:3px;"></td>
                            <td><?php echo $yaz->referans_tarihi; ?></td>
                            <td><a href="/yonetimpaneli/referansduzenle/<?php echo $yaz->id; ?>" >Düzenle</a> / <a href="javascript:;" onclick="sil(<?php echo $yaz->id; ?>);">Sil</a></td>
                            
                        </tr>
					   <?php } ?>
						
                    </tbody>
                  </table>
                </div>
                   
                    
                <!--end container-->
            </section>
			<div class="row">
           	<div class="col s4 m4 ">&nbsp;</div>
				
                <div class="col s4 m8 cpm">
				  
				   <?php echo $linkler; ?>
  
             </div>     
<div class="col s4 m4 "></div>
</div>			 
</div>
        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->
	 <style>
 
 .cpm a{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 margin-left:2px;
	 border-radius:3px;
	 color:#222;
 }
 
 .cpm strong{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 color:#00bcd4;
 }
 
 #data-table-simple{
	 
 }
 
 
 </style>

 <?php $this->load->view("yonetim/includes/footer"); ?>

    <script>
	
	function sil(id){
		    var txt;
    var r = confirm("Silmek istediğinize emin misiniz ?");
    if (r == true) {
        location.replace('/yonetimpaneli/referansil/'+id);
    } else {
       return false;
    }
			
			
		}
	
	
	
	</script>