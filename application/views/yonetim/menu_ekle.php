 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                  
					<div class="col s12 m8 l9">
                 
				 <div id="basic-form" class="section">
              <div class="row">
                <div class="col s12 m12 24">
				<?php echo $this->session->flashdata("basarili"); ?>
				<?php echo $this->session->flashdata("basarisiz"); ?>
                  <div class="card-panel">
                    <h4 class="header2">Menü Ekle</h4>
                    <div class="row">
                      <form class="col s12" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
						
					  
                          <div class="input-field col s12">
                            <input id="name" type="text" name="menuadi" required>
                            <label for="first_name">Menü Adı</label>
                          </div>
                        </div>
                       
                     <div class="select-wrapper initialized">
                     <select name="kategori[]" multiple>
					 <option value="" selected disabled>Kategori Seç</option>
					<?php foreach($kategoriler as $yaz) { ?> 
                    <option value="<?php echo $yaz->id; ?>"><?php echo $yaz->kategori_adi; ?></option>
					<?php } ?>
					</select>
					  </div>
                    <label for="first_name">Kategori Adı</label>
                       
                        
                        <div class="row">
                          
						  
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Kaydet
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
				 
				 
                </div>
                   
                    
               
            </section>
           

        </div>
       

    </div>
    <!-- END MAIN -->

 <?php $this->load->view("yonetim/includes/footer"); ?>  