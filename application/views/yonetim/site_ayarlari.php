 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                  
					<div class="col s12 m8 l9">
                 
				 <div id="basic-form" class="section">
              <div class="row">
			  <form action="" method="POST" enctype="multipart/form-data">
                <div class="col s12 m12 24">
				<?php echo $this->session->flashdata("basarili"); ?>
				<?php echo $this->session->flashdata("basarisiz"); ?>
                  <div class="card-panel">
                    <h4 class="header2">Site Ayarları</h4>
                    <div class="row">
                      <form class="col s12">
                        <div class="row">
						<div class="file-field input-field col s6" style="margin-bottom:20px;">
                                   
					<div class="resim"><img src="<?php echo base_url("/uploads/").$siteayarlari->site_logo; ?>" class="logoborder"></div>
						<input class="file-path validate" type="text" name="file">
						
                        <div class="btn">
                          <span>Logo Seç</span>
                          <input type="file" class="file" name="file">
                        </div>
                      </div>
					  <div class="file-field input-field col s6">
                 <div class="resim1"><img src="<?php echo base_url("/uploads/").$siteayarlari->site_favicon; ?> " class="favicon"></div>
						<input class="file-path validate" type="text" name="file1">
						
                        <div class="btn">
                          <span>Favicon Seç</span>
                          <input type="file" class="file1" name="file1">
                        </div>
                      </div>
                          <div class="input-field col s12">
                            <input id="name" type="text" name="siteadi" value="<?php echo $siteayarlari->site_adi; ?>" required>
                            <label for="first_name">Site Adı</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                            <textarea id="message" class="materialize-textarea" name="siteaciklamasi" required><?php echo $siteayarlari->site_aciklamasi; ?></textarea>
                            <label for="message">Site Açıklaması</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                            <input id="password" type="text" name="anahtarkelimeler" value="<?php echo $siteayarlari->site_anahtar; ?>" required>
                            <label for="password">Anahtar Kelimeler</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                            <input id="password" type="text" name="siteyazar" value="<?php echo $siteayarlari->yazar; ?>" required>
                            <label for="password">Site Yazar</label>
                          </div>
						  
                          
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Kaydet
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                         
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                </form>
              </div>
            </div>
				 
				 
                </div>
                   
                    
               
            </section>
           

        </div>
       

    </div>
    <!-- END MAIN -->

 <?php $this->load->view("yonetim/includes/footer"); ?>

    
	<script>
	
	 var preview = $(".file");
	 
	 var preview = $(".file1");
	 
     $(".file").change(function(event){
     var input = $(event.currentTarget);
     var file = input[0].files[0];
     var reader = new FileReader();
     reader.onload = function(e){
     image_base64 = e.target.result;
     $(".resim").html("<img src='"+image_base64+"' style='max-width:100px;'/>");
     };
     reader.readAsDataURL(file);
     });
	 
	  $(".file1").change(function(event){
     var input = $(event.currentTarget);
     var file = input[0].files[0];
     var reader = new FileReader();
     reader.onload = function(e){
     image_base64 = e.target.result;
     $(".resim1").html("<img src='"+image_base64+"' style='max-width:100px;'/>");
     };
     reader.readAsDataURL(file);
     });
	
	</script>