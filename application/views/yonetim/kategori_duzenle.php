 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                  
                  
					<div class="col s12 m8 l9">
                 
				 <div id="basic-form" class="section">
              <div class="row">
                <div class="col s12 m12 24">
				<?php echo $this->session->userdata("basarili"); ?>
                  <div class="card-panel">
                    <h4 class="header2">Kategori Düzenleme</h4>
                    <div class="row">
                       <form class="col s12" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
						
					 
                          <div class="input-field col s12">
                            <input id="name" type="text" value="<?php echo $kategori->kategori_adi; ?>" name="kategoriadi">
                            <label for="first_name">Kategori Adı</label>
                          </div>
						    
						  
                        </div>
                       
                       
                        <div class="row">
                       
						  
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Kaydet
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
				 
				 
                </div>
                   
                    
               
            </section>
           

        </div>
       

    </div>
    <!-- END MAIN -->

 <?php $this->load->view("yonetim/includes/footer"); ?>

    