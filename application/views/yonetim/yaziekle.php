 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                  
					<div class="col s12 m8 l9">
                 
				 <div id="basic-form" class="section">
              <div class="row">
                <div class="col s12 m12 24">
				
				<?php echo $this->session->flashdata("basarili"); ?>
                  <div class="card-panel">
                    <h4 class="header2">Yazı Ekle</h4>
                    <div class="row">
                      <form class="col s12" action="" method="POST" enctype="multipart/form-data">
					  <div class="row">
						<div class="file-field input-field col s12">
                         <div class="resim"></div> 
						<input class="file-path validate" type="text" name="file">
						
                        <div class="btn">
                          <span>Yazı Resmi Seç</span>
                          <input type="file" name="file" class="file" required="">
                        </div>
                      </div>
					  
                         
                        </div>
					  
                        <div class="row">
						
                          <div class="input-field col s12">
                            <input id="name" type="text" name="yazibaslik" required>
                            <label for="first_name">Yazı Başlık</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="input-field col s12">
                            <textarea name="editor_content" id="myEditor" required></textarea>
                            
                          </div>
                        </div>
						
						<div class="row">
						
                          <div class="input-field col s12">
                            <select name="kategori" required>
	
                    <option disabled selected>Kategori Seç</option>
					<?php foreach($kategoriler as $yaz) { ?> 
                    <option value="<?php echo $yaz->id; ?>"><?php echo $yaz->kategori_adi; ?></option>
                    <?php } ?>
					
                  </select>
                          </div>
                        </div>
						
						<div class="row">
						
                          <div class="input-field col s12">
                            <input id="name" type="text" name="tags">
                            <label for="first_name">Etiketler</label>
                          </div>
                        </div>
                        
                        <div class="row">
                          
						  
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Kaydet
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
				 
				 
                </div>
                   
                    
               
            </section>
           

        </div>
       

    </div>
    <!-- END MAIN -->

 <?php $this->load->view("yonetim/includes/footer"); ?>
 
 <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0//js/froala_editor.pkgd.min.js"></script>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.3/js/languages/tr.js"></script>

    <script>
	
	$(document).ready(function(){
		
		
		
		$('#myEditor').froalaEditor({
			language: 'tr',
			toolbarInline: false, 
			placeholderText: 'İçerik Giriniz...', 
			})
			
			
		
   
   
     var preview = $(".file");
 
     $(".file").change(function(event){
     var input = $(event.currentTarget);
     var file = input[0].files[0];
     var reader = new FileReader();
     reader.onload = function(e){
     image_base64 = e.target.result;
     $(".resim").html("<img src='"+image_base64+"' style='max-width:100px;'/>");
     };
     reader.readAsDataURL(file);
     });
     
   
  
		
	});
	
	</script>