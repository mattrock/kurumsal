 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

    
            <section id="content">

               
                <div class="container">

                   
                  <form action="/yonetimpaneli/hakkimizda" method="POST">
					
					<div class="col s12 m12 l6">
					<?php echo $this->session->userdata("basarili"); ?>
                  <div class="card-panel">
                    
                    
                      
                        <div class="row">
                          <div class="input-field col s12">
                            <input id="name" type="text" name="baslik" value="<?php echo $hakkimizda->baslik; ?>">
                            <label for="first_name" class="">Sayfa Başlığı</label>
                          </div>
                        </div>
                        
                       
                      
                   
                  </div>
                </div>
					
					<div class="col s12 m8 l9" style="margin-top:30px;">
					
                  <textarea name="editor_content" id="myEditor"><?php echo $hakkimizda->icerik; ?></textarea><br>
				  <button class="waves-effect waves-light  btn" name="kaydet" type="submit">Kaydet</button>
				  
                </div>
          </form>
            </section>
           

        </div>
      

    </div>
 

 <?php $this->load->view("yonetim/includes/footer"); ?>
 
 <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0//js/froala_editor.pkgd.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.3/js/languages/tr.js"></script>
    <script>
  
  $(document).ready(function(){
	  
	  
	  
	  $('#myEditor').froalaEditor({
		  language: 'tr',
		  toolbarInline: false
		  })
	  
  });
  
    
  
</script>

