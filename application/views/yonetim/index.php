 <?php $this->load->view("yonetim/includes/header"); ?>
<style>

.col.s6.m6 {
    border-right: 1px solid #e1e1e1;
}



</style>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                   
                    <div id="card-stats">
                        <div class="row">
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content  green white-text">
                                        <p class="card-stats-title"><i class="mdi-action-shopping-cart
"></i> Toplam Ürün</p>
                                        <h4 class="card-stats-number"><?php echo @$toplamurunler; ?> </h4>
                                        </p>
                                    </div>
                                    <div class="card-action  green darken-2">
                                        <div id="clients-bar" class="center-align"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content pink lighten-1 white-text">
                                        <p class="card-stats-title"><i class="mdi-editor-insert-drive-file"></i> Toplam Slider</p>
                                        <h4 class="card-stats-number"><?php echo $slider; ?></h4>
                                        
                                    </div>
                                    <div class="card-action  pink darken-2">
                                        <div id="invoice-line" class="center-align"></div>
                                    </div>
                                </div>
                            </div>
                           
							<div class="col s12 m6 l3">
							
							<div class="card">
                                    <div class="card-content purple white-text">
                                        <p class="card-stats-title"><i class="mdi-editor-insert-drive-file"></i>Toplam Kategori</p>
                                        <h4 class="card-stats-number"><?php echo $kategoritop; ?></h4>
                                        
                                    </div>
                                    <div class="card-action purple darken-2">
                                        <div id="sales-compositebar" class="center-align"><canvas width="214" height="25" style="display: inline-block; width: 214px; height: 25px; vertical-align: top;"></canvas></div>

                                    </div>
                                </div>
							
							</div>
							
							<div class="col s12 m6 l3">
							<div class="card">
                                    <div class="card-content blue-grey white-text">
                                        <p class="card-stats-title"><i class="mdi-action-trending-up"></i> Toplam Yazı</p>
                                        <h4 class="card-stats-number"><?php echo $toplamyazi; ?></h4>
                                        
                                    </div>
                                    <div class="card-action blue-grey darken-2">
                                        <div id="profit-tristate" class="center-align"><canvas width="227" height="25" style="display: inline-block; width: 227px; height: 25px; vertical-align: top;"></canvas></div>
                                    </div>
                                </div>
                                </div>
							
                        </div>
                    </div>
					
					<div class="row">
					<div class="col s6 m6">
                  <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Ürün İd </th> 
                            <th>Ürün Adı</th>
                            <th>Ürün Resmi</th>
							<th>Ürün Kategori</th>
                            <th>Tarihi</th>
                            
                            
                        </tr>
                    </thead>
                 
                
                 
                    <tbody>
					
					
					
                          <?php foreach($veriler as $yazdir) { ?>
						          <?php $kategoriesle = $this->sql->kategoriesle($yazdir->urun_kategori); 
								        $urunresimcek = $this->sql->urunp($yazdir->id);
										
										
										
										
								  if($kategoriesle == null){
									  $kategoriesle = array(
									  "kategori_adi" => "ds",
									  );
								  }
								  
								  
								  ?>
						  <?php foreach($kategoriesle as $yaz) { ?>
						           
									<tr> 
                                   
									
						
                            <td><?php echo $yazdir->id; ?></td>
                            <td><?php echo strip_tags($yazdir->urun_adi); ?></td>
                            
							
							 
							<td><img src="<?php echo base_url("uploads/") . $urunresimcek->urunresmi; ?>"></td>
                          
							
							
                           <td><?php echo @$son = strip_tags($yaz->kategori_adi); if($son==""){echo 'Kategori Ekleyin';} ?></td>
                            <td><?php echo $yazdir->urun_tarih; ?></td>
                        </tr>
						
									<?php  } } ?>
									
                    </tbody>
                  </table>
				   <div class="row">
				<div class="col s4 m4 ">&nbsp;</div>
				
                <div class="col s4 m8 cpm">
				  
				   
   
				 
  
  
             </div>     
<div class="col s4 m4 "></div>
</div>	
                </div>
<div class="col s6 m6">
                  <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                           
                            <th>Slider Başlık</th>
                            <th>Slider Resmi</th>
							<th>Slider Slogan</th>
                            <th>Tarihi</th>
                            
                            
                        </tr>
                    </thead>
                 
                
                 
                    <tbody>
                    
					   <?php foreach($sliderozet as $yazi) { ?>
					   
                        <tr>
                          
                            <td><?php echo $yazi->slider_adi; ?></td>
                            <td><img src="<?php echo base_url("uploads/") .$yazi->slider_resmi; ?>"></td>
                            <td><?php echo $yazi->slider_slogan; ?></td>
                            <td><?php echo $yazi->slider_tarih; ?></td>
                           
                            
                        </tr>
					   <?php } ?>
						
                    </tbody>
                  </table>
                </div> 				
				</div>
				
					 
            </section>
           

        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->

 <?php $this->load->view("yonetim/includes/footer"); ?>

 <style>
 
  #data-table-simple img{
	 width:50px;
	 height:50px;
	 
 }
 
 .cpm a{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 margin-left:2px;
	 border-radius:3px;
	 color:#222;
 }
 
 .cpm strong{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 color:#00bcd4;
 }
 

 
 
 </style>
 
 
    