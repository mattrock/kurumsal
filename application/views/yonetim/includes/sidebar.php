
<?php 


$bul = $_SERVER["REQUEST_URI"]; 

$id = $this->uri->segment(3);



?>




 <?php $uyebilgileri = $this->session->userdata("uyebilgi"); ?>
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="<?php echo base_url("uploads/"); ?><?php echo $uyebilgileri->resim; ?>" alt="" class="circle responsive-img valign profile-image" style="width:100px; height:50px;">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li class="<?php echo $bul == "/yonetimpaneli/profil" ? 'active' : ''; ?>"><a href="/yonetimpaneli/profil"><i class="mdi-action-face-unlock"></i> Profil</a>
                            </li>

                            <li class="<?php echo $bul == "/yonetimpaneli/cikis" ? 'active' : ''; ?>"><a href="/yonetimpaneli/cikis"><i class="mdi-hardware-keyboard-tab"></i> Çıkış</a>
                            </li>
                        </ul>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?php echo $uyebilgileri->adsoyad; ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal">Yönetici</p>
                    </div>
                </div>
                </li>
                <li class="bold <?php echo $bul == "/yonetimpaneli/index" ? 'active' : ''; ?>"><a href="/yonetimpaneli/index"  class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Özet</a>
                </li>
                <li class="bold <?php echo $bul == "/yonetimpaneli/siteayarlari" ? 'active' : ''; ?>"><a href="/yonetimpaneli/siteayarlari"  class="waves-effect waves-cyan"><i class="mdi-action-settings"></i> Site Ayarları</a>
                </li>
				<li class="bold <?php echo $bul == "/yonetimpaneli/menuayarlari" || $bul == "/yonetimpaneli/menuduzenle/$id" ? 'active' : ''; ?>"><a href="/yonetimpaneli/menuayarlari"    class="waves-effect waves-cyan"><i class="mdi-navigation-menu"></i> Menü Ayarları</a>
				
				<div class="collapsible-body" <?php echo $bul == "/yonetimpaneli/menuayarlari" || $bul == "/yonetimpaneli/menuekle" || $bul == "/yonetimpaneli/menuduzenle/$id"  ? 'style="display:block;"' : ''; ?>>
                            <ul>
                                <li class="<?php echo $bul == "/yonetimpaneli/menuekle" ? 'active' : ''; ?>"><a href="/yonetimpaneli/menuekle">Menü Ekle</a>
                                </li>
                            </ul>
                        </div>
                </li>
                <li class="bold <?php echo $bul == "/yonetimpaneli/slider" || $bul == "/yonetimpaneli/sliderduzenle/$id"  ? 'active' : ''; ?>"><a href="/yonetimpaneli/slider" class="waves-effect waves-cyan"><i class="mdi-image-slideshow"></i> Slider</a>
				<div class="collapsible-body" <?php echo $bul == "/yonetimpaneli/slider" || $bul == "/yonetimpaneli/sliderekle" || $bul == "/yonetimpaneli/sliderduzenle/$id" ? 'style="display:block;"' : ''; ?>>
                            <ul>
                              
                                <li class="<?php echo $bul == "/yonetimpaneli/sliderekle" ? 'active' : ''; ?>"><a href="/yonetimpaneli/sliderekle">Slider Ekle</a>
                                </li>
                            </ul>
                        </div>
                </li>
				<li class="bold <?php echo $bul == "/yonetimpaneli/urunler" || $bul == "/yonetimpaneli/urunduzenle/$id" || $bul == "/yonetimpaneli/kategoriduzenle/$id" ? 'active' : ''; ?>"><a href="/yonetimpaneli/urunler" class="waves-effect waves-cyan"><i class="mdi-action-store"></i> Ürünler</a>
                <div class="collapsible-body" <?php echo $bul == "/yonetimpaneli/urunler" || $bul == "/yonetimpaneli/urunekle" || $bul == "/yonetimpaneli/kategoriler" || $bul == "/yonetimpaneli/urunduzenle/$id" || $bul == "/yonetimpaneli/kategoriduzenle/$id"  ? 'style="display:block;"' : ''; ?>>
                            <ul>
                              
                                <li class="<?php echo $bul == "/yonetimpaneli/urunekle" ? 'active' : ''; ?>"><a href="/yonetimpaneli/urunekle">Ürün Ekle</a></li>
                                <li class="<?php echo $bul == "/yonetimpaneli/kategoriler" ? 'active' : ''; ?>"><a href="/yonetimpaneli/kategoriler">Kategoriler</a></li>
                            </ul>
                        </div>
				</li>
				<li class="bold <?php echo $bul == "/yonetimpaneli/blog" || $bul == "/yonetimpaneli/blogyaziduzenle/$id" || $bul == "/yonetimpaneli/blogkategoriduzenleme/$id"  ? 'active' : ''; ?> "><a href="/yonetimpaneli/blog" class="waves-effect waves-cyan"><i class="mdi-image-add-to-photos
"></i> Blog</a>
                <div class="collapsible-body" <?php echo $bul == "/yonetimpaneli/blog" || $bul == "/yonetimpaneli/yaziekle" || $bul == "/yonetimpaneli/blogkategori" || $bul == "/yonetimpaneli/blogyaziduzenle/$id" || $bul == "/yonetimpaneli/blogkategoriduzenleme/$id" ? 'style="display:block;"' : ''; ?>>
                            <ul>
                              
                                <li class="<?php echo $bul == "/yonetimpaneli/yaziekle" ? 'active' : ''; ?>"><a href="/yonetimpaneli/yaziekle">Yazı Ekle</a></li>
                                <li class="<?php echo $bul == "/yonetimpaneli/blogkategori" ? 'active' : ''; ?>"><a href="/yonetimpaneli/blogkategori">Kategoriler</a></li>
                            </ul>
                        </div>
				</li>
				
				<li class="bold <?php echo $bul == "/yonetimpaneli/referanslar" || $bul == "/yonetimpaneli/referansduzenle/$id" ? 'active' : ''; ?> "><a href="/yonetimpaneli/referanslar" class="waves-effect waves-cyan"><i class="mdi-image-photo

"></i> Referanslar</a>
                <div class="collapsible-body" <?php echo $bul == "/yonetimpaneli/referanslar" || $bul == "/yonetimpaneli/referansekle" || $bul == "/yonetimpaneli/referansduzenle/$id" ? 'style="display:block;"' : ''; ?>>
                            <ul>
                              
                                <li class="<?php echo $bul == "/yonetimpaneli/referansekle" ? 'active' : ''; ?>"><a href="/yonetimpaneli/referansekle">Referans Ekle</a></li>
                            </ul>
                        </div>
				</li>
				
				<li class="bold <?php echo $bul == "/yonetimpaneli/haberbulteni" ? 'active' : ''; ?> "><a href="/yonetimpaneli/haberbulteni" class="waves-effect waves-cyan"><i class="mdi-communication-email

"></i> Haber Bülteni</a>
                <div class="collapsible-body" <?php echo $bul == "/yonetimpaneli/haberbulteni" || $bul == "/yonetimpaneli/haberbulteniekle" ? 'style="display:block;"' : ''; ?>>
                            <ul>
                              
                                <li class="<?php echo $bul == "/yonetimpaneli/haberbulteniekle" ? 'active' : ''; ?>"><a href="/yonetimpaneli/haberbulteniekle">Abone Ekle</a></li>
                            </ul>
                        </div>
				</li>
				
				<li class="bold <?php echo $bul == "/yonetimpaneli/hakkimizda" ? 'active' : ''; ?>"><a href="/yonetimpaneli/hakkimizda" class="waves-effect waves-cyan"><i class="mdi-action-assignment"></i> Hakkımızda</a>
                </li>
				<li class="bold <?php echo $bul == "/yonetimpaneli/iletisim" ? 'active' : ''; ?>"><a href="/yonetimpaneli/iletisim" class="waves-effect waves-cyan"><i class="mdi-action-perm-contact-cal"></i> İletişim</a>
                </li>
                <li class="bold <?php echo $bul == "/yonetimpaneli/cikis" ? 'active' : ''; ?>"><a href="/yonetimpaneli/cikis" class="waves-effect waves-cyan"><i class="mdi-action-exit-to-app"></i> Çıkış</a>
                </li>
               
               
            </ul>
                <a href="/yonetimpaneli/cikis" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
            </aside>