 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                        <div class="col s12 m4" style="margin-top:30px; margin-bottom:30px;">
                          <a class="btn-floating btn-large waves-effect waves-light " href="/yonetimpaneli/haberbulteniekle"><i class="mdi-content-add"></i></a>
                         <a class="dropdown-button btn-floating btn-flat waves-effect waves-light  pink accent-2 white-text" href="#!" data-activates="dropdown2"><i class="mdi-file-cloud-download"></i></a>
                        <ul id="dropdown2" class="dropdown-content" style="width: 37px; position: absolute; top: 1306.78px; left: 1215.92px; opacity: 1; display: none;">
                            <li><a href="/yonetimpaneli/excelaktar" class="-text">Excel</a>
                            </li>
                            <li><a href="/yonetimpaneli/metinaktar" class="-text">Metin</a>
                            </li>
                          </ul>
						</div>
                        
                      
   
					
					
					<div class="col s12 m8 l9">
					<?php echo $this->session->flashdata("basarili"); ?>
                  <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Abone id</th>
                            <th>Abone Mail</th>
                            <th>Abone Tarihi</th>
                            <th>İşlemler</th>
                            
                        </tr>
                    </thead>
                 
                
                 
                    <tbody>
                       
					   
					   <?php foreach($veriler as $yaz) { ?>
                        <tr>
                            <td><?php echo $yaz->id; ?></td>
                            <td><?php echo $yaz->mail_adresi; ?></td>
                            <td><?php echo $yaz->tarih; ?></td>
                            <td><a href="javascript:;" onclick="sil(<?php echo $yaz->id; ?>);">Sil</a></td>
                            
                        </tr>
					   <?php } ?>
						
                    </tbody>
                  </table>
                </div>
                   
                    
                <!--end container-->
            </section>
			<div class="row">
           	<div class="col s4 m4 ">&nbsp;</div>
				
                <div class="col s4 m8 cpm">
				  
				   <?php echo $linkler; ?>
  
             </div>     
<div class="col s4 m4 "></div>
</div>			 
</div>
        </div>
        <!-- END WRAPPER -->

    </div>
    <!-- END MAIN -->
	 <style>
 
 .cpm a{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 margin-left:2px;
	 border-radius:3px;
	 color:#222;
 }
 
 .cpm strong{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 color:#00bcd4;
 }
 
 #data-table-simple{
	 
 }
 
 
 </style>

 <?php $this->load->view("yonetim/includes/footer"); ?>

    <script>
	
	function sil(id){
		    var txt;
    var r = confirm("Silmek istediğinize emin misiniz ?");
    if (r == true) {
        location.replace('/yonetimpaneli/abonesil/'+id);
    } else {
       return false;
    }
			
			
		}
	
	
	
	</script>