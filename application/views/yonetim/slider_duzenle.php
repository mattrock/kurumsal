 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   
                  
					<div class="col s12 m8 l9">
                  
				 <div id="basic-form" class="section">
              <div class="row">
                <div class="col s12 m12 24">
				<?php echo $this->session->flashdata("basarili"); ?>
				<?php echo $this->session->flashdata("basarisiz"); ?>
				
                  <div class="card-panel">
                    <h4 class="header2">Slider Düzenleme</h4>
                    <div class="row">
                      <form class="col s12" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
						 
						 
						
						<div class="file-field input-field col s12">
 <div class="resim"><img src="<?php echo base_url("uploads/").$slider->slider_resmi; ?>" style="width:100px; height:100px;"></div>
						<input class="file-path validate" type="text">
                        <div class="btn">
                          <span>Slider Resmi Düzenle</span>
                          <input type="file" class="file" name="file">
                        </div>
                      </div>
					 
                          <div class="input-field col s12">
                            <input id="name" type="text" name="sld_baslik" value="<?php echo $slider->slider_adi; ?>" required>
                            <label for="first_name">Slider Başlık Düzenle</label>
                          </div>
						  <div class="input-field col s12">
                            <input id="name" type="text" name="sld_duzenle" value="<?php echo $slider->slider_slogan; ?>" required>
                            <label for="first_name">Slogan Düzenle</label>
                          </div>
                        </div>
                       
                       
                        <div class="row">
                          
						  
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Kaydet
                                <i class="mdi-content-send right"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
				 
				 
                </div>
                   
                    
               
            </section>
           

        </div>
       

    </div>
    

 <?php $this->load->view("yonetim/includes/footer"); ?>

    <script>
	
	 var preview = $(".file");
     $(".file").change(function(event){
     var input = $(event.currentTarget);
     var file = input[0].files[0];
     var reader = new FileReader();
     reader.onload = function(e){
     image_base64 = e.target.result;
     $(".resim").html("<img src='"+image_base64+"' style='max-width:100px;'/>");
     };
     reader.readAsDataURL(file);
     });
	
	</script>