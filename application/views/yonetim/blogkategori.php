 <?php $this->load->view("yonetim/includes/header"); ?>
 <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

           <?php $this->load->view("yonetim/includes/sidebar"); ?>

            <!-- //////////////////////////////////////////////////////////////////////////// -->

            <!-- START CONTENT -->
            <section id="content">

                <!--start container-->
                <div class="container">

                   <form action="" method="POST">
                        <div class="col s6 m4" style="margin-top:30px; margin-bottom:30px;">
						<?php echo $this->session->flashdata("basarili"); ?>
						<?php echo $this->session->flashdata("silindi"); ?>
                          <div class="row">
                        <div class="input-field col s6">
                          <input id="input_text" type="text" length="10" name="kategoriadi">
                          <label for="input_text">Kategori Adı</label>
                        <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
						<button style="margin-top:20px" class="btn waves-effect waves-light " type="submit" name="action">Kategori Ekle
                        <i class="mdi-content-send right"></i>
                      </button>
                      </div>
                         
                        </div>
                        </form>
                      
   
					
					
					<div class="col s12 m8 l9">
                  <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Kategori id</th>
                            <th>Kategori Adı</th>
                            <th>İşlemler</th>
                            
                        </tr>
                    </thead>
                 
                
                 
                    <tbody>
                       <?php foreach($veriler as $yaz) { ?>
                        <tr>
                            <td><?php echo $yaz->id; ?></td>
                            <td><?php echo strip_tags($yaz->kategori_adi); ?></td>
                            <td><a href="/yonetimpaneli/blogkategoriduzenleme/<?php echo $yaz->id; ?>">Düzenle</a> / <a href="javascript:;" onclick="sil(<?php echo $yaz->id; ?>);">Sil</a></td>
                            
                        </tr>
					   <?php } ?>
                    </tbody>
                  </table>
                </div>
                   <div class="row">
				<div class="col s4 m4 ">&nbsp;</div>
				
                <div class="col s4 m8 cpm">
				  
				   <?php echo $linkler; ?>
   
				 
  
  
             </div>     
<div class="col s4 m4 "></div>
</div>			 
               
            </section>
           

        </div>
        

    </div>
    <style>
 
 .cpm a{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 margin-left:2px;
	 border-radius:3px;
	 color:#222;
 }
 
 .cpm strong{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 color:#00bcd4;
 }
 
 #data-table-simple{
	 
 }
 
 
 </style>

 <?php $this->load->view("yonetim/includes/footer"); ?>

    <script>
	
	function sil(id){
		    var txt;
    var r = confirm("Silmek istediğinize emin misiniz ?");
    if (r == true) {
        location.replace('/yonetimpaneli/blogkategorisilme/'+id);
    } else {
       return false;
    }
			
			
		}
	
	
	
	</script>