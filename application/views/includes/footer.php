<div id="footer-wrapper" class="footer-dark">
            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <ul class="col-md-3 col-sm-6 footer-widget-container clearfix">
                            <!-- .widget.widget_text -->
                            <li class="widget widget_newsletterwidget">
                                <div class="title">
                                    <h3>Hakkımızda</h3>
                                </div>

                                <p>
                                  <?php $yaz = strip_tags(htmlspecialchars_decode($hakkimizdacek->icerik)); echo mb_substr($yaz,0,150) . "..."; ?>
                                </p>

                                <br />

                              
                            </li><!-- .widget.widget_newsletterwidget end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3>HIZLI lİnkler</h3>
                                </div>

                                <ul>
                                    <li><a href="<?php echo base_url("/") . "hakkimizda" ?>">Hakkımızda</a></li>
                                    <li><a href="<?php echo base_url("/") . "galeri" ?>">Galeri</a></li>
                                  
                                    
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <!-- .widget-pages start -->
                            <li class="widget widget_pages">
                                <div class="title">
                                    <h3>Hİzmetler</h3>
                                </div>

                                <ul>
                                     <?php foreach($blogkategori as $yaz) { ?>
                                                               <li><a href="<?php echo base_url("kategori/") . $yaz->id; ?>"><?php echo $yaz->kategori_adi ?></a></li>
															   <?php } ?>
                                </ul>
                            </li><!-- .widget-pages end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->

                        <ul class="col-md-3 col-sm-6 footer-widget-container">
                            <li class="widget widget-text">
                                <div class="title">
                                    <h3>bİzİmle İletİşİme geçİn</h3>
                                </div>

                                <address>
                                    <?php echo $iletisimcekek->adres; ?>
                                </address>

                                <span class="text-big">
                                    <?php echo $iletisimcekek->tel; ?>
                                </span>
                                <br />

                                <a href="mailto:<?php echo $iletisimcekek->mail; ?>"><?php echo $iletisimcekek->mail; ?></a>
                                <br />
                                <ul class="footer-social-icons">
                                    <li><a href="<?php echo $iletisimcekek->fb_adresi; ?>" class="fa fa-facebook"></a></li>
                                    <li><a href="<?php echo $iletisimcekek->tw_adresi; ?>" class="fa fa-twitter"></a></li>
                                    <li><a href="<?php echo $iletisimcekek->g_adresi; ?>" class="fa fa-google-plus"></a></li>
                                </ul><!-- .footer-social-icons end -->
                            </li><!-- .widget.widget-text end -->
                        </ul><!-- .col-md-3.footer-widget-container end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </footer><!-- #footer end -->

            <div class="copyright-container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
© 2017 Tüm hakları saklıdır.</p>
                        </div><!-- .col-md-6 end -->

                        <div class="col-md-6">
                            <p class="align-right">| ... Tasarım :<a href="http://www.kodifix.com/" target="_blank"> Kodifix Bilişim Hizmetleri</a></p> 
                        </div><!-- .col-md-6 end -->
                    </div><!-- .row end -->
                </div><!-- .container end -->
            </div><!-- .copyright-container end -->

            <a href="#" class="scroll-up">Scroll</a>
        </div><!-- #footer-wrapper end -->

        <script src="<?php echo base_url("assets/frontend/"); ?>js/jquery-2.1.4.min.js"></script><!-- jQuery library -->
        <script src="<?php echo base_url("assets/frontend/"); ?>js/bootstrap.min.js"></script><!-- .bootstrap script -->
        <script src="<?php echo base_url("assets/frontend/"); ?>js/jquery.srcipts.min.js"></script><!-- modernizr, retina, stellar for parallax -->  
        <script src="<?php echo base_url("assets/frontend/"); ?>owl-carousel/owl.carousel.min.js"></script><!-- Carousels script -->
        <script src="<?php echo base_url("assets/frontend/"); ?>masterslider/masterslider.min.js"></script><!-- Master slider main js -->
        <script src="<?php echo base_url("assets/frontend/"); ?>js/jquery.matchHeight-min.js"></script><!-- for columns with background image -->
        <script src="<?php echo base_url("assets/frontend/"); ?>js/jquery.dlmenu.min.js"></script><!-- for responsive menu -->
        <script src="<?php echo base_url("assets/frontend/"); ?>style-switcher/styleSwitcher.js"></script><!-- styleswitcher script -->
        <script src="<?php echo base_url("assets/frontend/"); ?>js/include.js"></script><!-- custom js functions -->
        <script src="<?php echo base_url("assets/frontend/"); ?>js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url("assets/frontend/"); ?>js/portfolio.js"></script>
        <script src="<?php echo base_url("assets/frontend/"); ?>style-switcher/styleSwitcher.js"></script>
       
        <script>
            /* <![CDATA[ */
            jQuery(document).ready(function ($) {
                'use strict';

                function equalHeight() {
                    $('.page-content.column-img-bkg *[class*="custom-col-padding"]').each(function () {
                        var maxHeight = $(this).outerHeight();
                        $('.page-content.column-img-bkg *[class*="img-bkg"]').height(maxHeight);
                    });
                };
                
                $(document).ready(equalHeight);
                $(window).resize(equalHeight);

                // MASTER SLIDER START 
                var slider = new MasterSlider();
                slider.setup('masterslider', {
                    width: 1140, // slider standard width
                    height: 854, // slider standard height
                    space: 0,
                    speed: 50,
                    layout: "fullwidth",
                    centerControls: false,
                    loop: true,
                    autoplay: true
                            // more slider options goes here...
                            // check slider options section in documentation for more options.
                });
                // adds Arrows navigation control to the slider.
                slider.control('arrows');

                // CLIENTS CAROUSEL START 
                $('#client-carousel').owlCarousel({
                    items: 6,
                    loop: true,
                    margin: 30,
                    responsiveClass: true,
                    mouseDrag: true,
                    dots: false,
                    responsive: {
                        0: {
                            items: 2,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true
                        },
                        600: {
                            items: 3,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true
                        },
                        1000: {
                            items: 6,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            mouseDrag: true
                        }
                    }
                });

                // TESTIMONIAL CAROUSELS START
                $('#testimonial-carousel').owlCarousel({
                    items: 1,
                    loop: true,
                    margin: 30,
                    responsiveClass: true,
                    mouseDrag: true,
                    dots: false,
                    autoheight: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            autoHeight: true
                        },
                        600: {
                            items: 1,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            autoHeight: true
                        },
                        1000: {
                            items: 1,
                            nav: true,
                            loop: true,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            mouseDrag: true,
                            autoHeight: true
                        }
                    }
                });
            });
            /* ]]> */
        </script>
		
		
    </body>

</html>
