﻿<?php 
$directoryURI = $_SERVER['REQUEST_URI'];
$path = parse_url($directoryURI, PHP_URL_PATH);
$components = explode('/', $path);
$first_part = $components[1];
?>
<!DOCTYPE html>
<html>
    
<head>
        <title><?php echo $siteayarlari->site_adi; ?> </title>
        <meta name="description" content="<?php echo $siteayarlari->site_aciklamasi; ?>">
        <meta name="author" content="<?php echo $siteayarlari->yazar; ?>">
        <meta name="keywords" content="<?php echo $siteayarlari->site_anahtar; ?>" >
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		
        
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>css/bootstrap.css"/><!-- bootstrap grid -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>masterslider/style/masterslider.css" /><!-- Master slider css -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>masterslider/skins/default/style.css" /><!-- Master slider default skin -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>css/animate.css"/><!-- animations -->
        <link rel='stylesheet' href='<?php echo base_url("assets/frontend/"); ?>owl-carousel/owl.carousel.css'/><!-- Client carousel -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>css/style.css"/><!-- template styles -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>css/color-default.css"/><!-- template main color -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>css/retina.css"/><!-- retina ready styles -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>css/responsive.css"/><!-- responsive styles -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>css/magnific-popup.css"/><!-- responsive styles -->

        <link rel="Shortcut Icon" href="<?php echo base_url("uploads/") . $siteayarlari->site_favicon; ?>" type="image/x-icon">
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,800,700,600' rel='stylesheet' type='text/css'>

        <link rel="Shortcut Icon" href="<?php echo base_url("uploads/") . $siteayarlari->site_favicon; ?>" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>icon-fonts/font-awesome-4.3.0/css/font-awesome.min.css"/><!-- Fontawesome icons css -->
        <link rel="stylesheet" href="<?php echo base_url("assets/frontend/"); ?>style-switcher/styleSwitcher.css"/><!-- styleswitcher -->
    </head>
    
    <body>      

        <div class="header-wrapper header-transparent">
            <!-- .header.header-style01 start -->
            <header id="header"  class="header-style01">
                <!-- .container start -->
                <div class="container">
                    <!-- .main-nav start -->
                    <div class="main-nav">
                        <!-- .row start -->
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="navbar navbar-default nav-left" role="navigation">

                                    <!-- .navbar-header start -->
                                    <div class="navbar-header">
                                        <div class="logo">
                                            <a href="<?php echo base_url(""); ?>">
                                                <img src="<?php echo base_url("uploads/") . $siteayarlari->site_logo; ?>" alt="<?php echo $siteayarlari->site_adi; ?>"/>
                                            </a>
                                        </div><!-- .logo end -->
                                    </div><!-- .navbar-header start -->

                                    <!-- MAIN NAVIGATION -->
                                    <div class="collapse navbar-collapse">
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown <?php if ($first_part== "") {echo "current-menu-item"; } else  {}?>">
                                                <a href="<?php echo base_url(""); ?>"  >Anasayfa</a>   
                                            </li><!-- .dropdown end -->

                                            <li class="dropdown <?php if ($first_part=="hakkimizda") {echo "current-menu-item"; } else  {}?>">
                                                <a href="<?php echo base_url("/") . "hakkimizda" ?>">Hakkımızda</a> 
                                            </li><!-- .dropdown end -->

                                            <li class="dropdown <?php if ($first_part=="hizmetler") {echo "current-menu-item"; } else  {}?>">
                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">HİZMETLER</a>
                                                
                                                    
                                                         <ul class="dropdown-menu">
                                                        
                                                           <?php foreach($blogkategori as $yaz) { ?>
                                                               <li class=""><a href="<?php echo base_url("kategori/") .$yaz->id; ?>"><?php echo $yaz->kategori_adi ?></a></li>
															   <?php } ?>
                                                        
                                                    </ul>
  
                                                
                                            </li><!-- .dropdown end -->

                                            <li class="dropdown <?php if ($first_part=="galeri") {echo "current-menu-item"; } else  {}?>">
                                                <a href="<?php echo base_url("/") . "galeri"  ?>">galerİ</a>
                                            </li><!-- .dropdown end -->

                                        
                                            <li class="dropdown <?php if ($first_part=="iletisim") {echo "current-menu-item"; } else  {}?>">
                                                <a href="<?php echo base_url("/") . "iletisim"; ?>">İletİŞİm</a>   
                                                </ul><!-- .dropdown-menu end -->
                                            </li><!-- .dropdown end -->
                                        </ul><!-- .nav.navbar-nav end -->

                                        <!-- RESPONSIVE MENU -->
                                        <div id="dl-menu" class="dl-menuwrapper">
                                            <button class="dl-trigger">Menü</button>

                                            <ul class="dl-menu">
                                                <li>
                                                    <a href="<?php echo base_url(""); ?>">Anasayfa</a>
                                                </li>

                                                <li>
                                                    <a href="<?php echo base_url("/") . "hakkimizda" ?>">Hakkımızda</a>
                                                   
                                                </li>

                                                <li>
                                                    <a href="#">Hİzmetler</a>
                                                    <ul class="dl-submenu">
                                                        <li><a href="<?php echo base_url("/") . "hizmetler"  ?>">Hizmetlere Genel Bakış</a></li>
                                                        
                                                            
                                                            
															<?php foreach($blogkategori as $yaz) { ?>
                                                               <li><a href="<?php echo base_url("kategori/") .$yaz->id; ?>"><?php echo $yaz->kategori_adi ?></a></li>
															   <?php } ?>
                                                           
                                                        </li>
                                                        
                                                    </ul><!-- dl-submenu end -->
                                                </li>

                                                <li>
                                                    <a href="<?php echo base_url("/") . "Galeri"  ?>">Galerİ</a>
                                                </li>

                                                
                                                <li>
                                                   <a href="<?php echo base_url("/") . "iletisim"; ?>">İletİşİm</a>   
                                                    
                                                </li>
                                            </ul><!-- .dl-menu end -->
                                        </div><!-- #dl-menu end -->

                                        
                                    </div><!-- MAIN NAVIGATION END -->
                                </nav><!-- .navbar.navbar-default end -->
                            </div><!-- .col-md-12 end -->
                        </div><!-- .row end -->
                    </div><!-- .main-nav end -->
                </div><!-- .container end -->
            </header><!-- .header.header-style01 -->
        </div><!-- .header-wrapper end -->
		
		