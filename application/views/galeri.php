
        <div class="page-title-style01 page-title-negative-top pt-bkg04">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Galeri</h1>

                        <div class="breadcrumb-container">
                            <ul class="breadcrumb clearfix">
                                
                            </ul><!-- .breadcrumb end -->
                        </div><!-- .breadcrumb-container end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-title-style01.page-title-negative-top end -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="custom-heading02">
                            <h2>Araç Serisi Ve Nakliyecilik</h2>
                            <p>
           
                            </p>
                        </div><!-- .custom-heading02 end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content end -->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <ul class="vehicle-gallery clearfix">
					<?php foreach($veriler as $yaz) { ?>
					<li class="col-md-4">
                            <figure class="gallery-item-container">                               
                                <div class="gallery-item">
                                    <img src="<?php echo base_url("uploads/") . $yaz->urun_resmi; ?>" alt="<?php echo $yaz->urun_adi; ?>"/>

                                    <div class="hover-mask-container">
                                        <div class="hover-mask"></div>
                                        <div class="hover-zoom">
                                            <a href="<?php echo base_url("uploads/") . $yaz->urun_resmi; ?>" class="triggerZoom fa fa-search"></a>
                                        </div><!-- .hover-details end -->
                                    </div><!-- .hover-mask-container end -->
                                </div><!-- .service-item end -->

                                <figcaption>
                                    <h3><?php echo $yaz->urun_adi; ?></h3>
                                </figcaption>
                            </figure>
                        </li>
					<?php } ?>
					

                
                                              
                    </ul><!-- #vehicle-gallery end -->
                </div><!-- .row end -->
				<div class="row">
<div class="container">
<ul class="pagination">
<?php echo $linkler; ?>
</ul>
</div>		
</div>	
            </div><!-- .container end -->
        </div><!-- .page-content end -->

        <div class="page-content parallax parallax01 dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="call-to-action clearfix">
                            <div class="text">
                                <h2> Bizimle çalışmak ister misiniz? Çevrimiçi başvurun!</h2>
                                <p>
                                   
Profesyonel ve güvenilir şirketimizde çalışmak için acele edin. 
                                </p>                              
                            </div><!-- .text end -->

                            <a href="<?php echo base_url("/iletisim"); ?>" class="btn btn-big">
                                <span>hemen başvur</span>
                            </a>
                        </div><!-- .call-to-action end -->
                    </div><!-- .col-md-12 end -->
                </div><!-- .row end -->
            </div><!-- .container end -->
        </div><!-- .page-content.parallax end -->

