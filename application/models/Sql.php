<?php 

Class sql extends CI_Model{

function admin($kuladi,$sifre){
	$result = $this->db->select("*")
	->from("admin")
	->where("kuladi",$kuladi)
	->where("sifre",$sifre)
	->get()
	->row();
	 
	 return $result;
	
}

function urunler(){
	$result = $this->db->select("*")
	->from("urunler")
	->get()
	->result();
	return $result;
}

function toplamurunler(){
	$result = $this->db->select("*")
	->from("urunler")
	->get()
	->result();
	return count($result);
}

function siteayarekle($data){
	$result = $this->db->update("siteayarlari",$data);
	return $result;
}

function siteayarcek(){
	$result = $this->db->select("*")
	->from("siteayarlari")
	->get()
	->row();
	return $result;
}

function slidercek(){
	$result = $this->db->select("*")
	->from("sliderlar")
	->get()
	->result();
	return $result;
}

function slidercektek($id){
	$result = $this->db->select("*")
	->from("sliderlar")
	->where("id",$id)
	->get()
	->row();
	return $result;
}


function sliderekle($data){
	$result = $this->db->insert("sliderlar",$data);
	return $result;
}

function sliderguncelle($id){
	 $result = $this->db->select("*")
	 ->from("sliderlar")
	 ->where("id",$id)
	 ->get()
	 ->row();
	 return $result;
}

function sliderupdate($id,$data){
	$this->db->where("id",$id);
	$result = $this->db->update("sliderlar",$data);
	return $result;
}

function slideraresim($id){
	$result = $this->db->select("slider_resmi")
	->from("sliderlar")
	->where("id",$id)
	->get()
	->row();
	return $result;
}

function slidersil($id){
	$result = $this->db->delete("sliderlar",array("id" => $id));
	 return $result;
}
function uruncek($id){
	$result = $this->db->select("*")
	->from("urunler")
	->where("id",$id)
	->get()
	->row();
	return $result;
	
}

function urunresim($id){
	$result = $this->db->select("*")
	->from("urunresimleri")
	->where("urun_id",$id)
	->get()
	->result();
	return $result;
}


function urunupdate($id,$data){
	$this->db->where("id",$id);
	$result = $this->db->update("urunler",$data);
	return $result;
}

function urunsil($id){
	$result = $this->db->delete("urunler",array("id" => $id));
	 return $result;
}

function urunekle($data){
	$this->db->insert("urunler",$data);
	$last_id = $this->db->insert_id();
    return $last_id;
}

function kategoriekle($data){
	$result = $this->db->insert("kategoriler",$data);
	return $result;
}

function kategorilist(){
	$result = $this->db->select("*")
	->from("kategoriler")
	->get()
	->result();
	return $result;
}

function kategoriduzenle($id,$data){
	$this->db->where("id",$id);
	$result = $this->db->update("kategoriler",$data);
	return $result;
}

function kategoricek($id){
	$result = $this->db->select("*")
	->from("kategoriler")
	->where("id",$id)
	->get()
	->row();
	return $result;
}

function kategoriesle($id){
	$result = $this->db->select("*")
	->from("kategoriler")
	->where("id",$id)
	->get()
	->result();
	return $result;
}

function kategorisil($id){
	$result = $this->db->delete("kategoriler",array("id" => $id));
	 return $result;
}

function hakkimizdacek(){
	$result = $this->db->select("*")
	->from("hakkimizda")
	->where("id",1)
	->get()
	->row();
	return $result;
}

function hakkimizdaup($icerik,$la){
	 $this->db->where("id",1);
	 $result = $this->db->update("hakkimizda",array("icerik" =>  $icerik,"baslik" => $la , "seflink" => permalink($la)));
	 return $result;
}

function iletisimcek(){
	$result = $this->db->select("*")
	->from("iletisim")
	->where("id",1)
	->get()
	->row();
	return $result;
}

function iletisimup($data){
	 $this->db->where("id",1);
	 $result = $this->db->update("iletisim",$data);
	 return $result;
}

function toplamslider(){
	$result = $this->db->select("*")
	->from("sliderlar")
	->get()
	->result();
	return count($result);
}

function toplamkategori(){
	$result = $this->db->select("*")
	->from("kategoriler")
	->get()
	->result();
	return count($result);
}

function toplamyazi(){
	$result = $this->db->select("*")
	->from("blog")
	->get()
	->result();
	return count($result);
}

function profilcek(){
	$result = $this->db->select("*")
	->from("admin")
	->where("id",1)
	->get()
	->row();
	return $result;
	
	
}

function profilupdate($data){
	$this->db->where("id",1);
	$result = $this->db->update("admin",$data);
	return $result;
}

function profilresimcek(){
	 $result = $this->db->select("resim")
	 ->from("admin")
	 ->where("id",1)
	 ->get()
	 ->row();
	 return $result;
}


function listele($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("urunler");
		
		return $veriler->result(); 
	}

function listele1($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("kategoriler"); 
		return $veriler->result(); 
	}
	
	function listele2($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("sliderlar"); 
		return $veriler->result(); 
	}
	
	function sliderozet(){
	$result = $this->db->query("SELECT * from sliderlar order by id Desc Limit 0,10");
	return $result->result();
	}
	
	
	function siteayarlari(){
		$result = $this->db->select("*")
		->from("siteayarlari")
		->where("id",1)
		->get()
		->row();
		return $result;
	}
	
	function sliderlist(){
		$result = $this->db->select("*")
		->from("sliderlar")
		->get()
		->result();
		return $result;
	}

	function faviconer(){
		$result = $this->db->select("*")
		->from("siteayarlari")
		->where("id",1)
		->get()
		->row();
		return $result;
	}
	
	 function listele3($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("blog"); 
		return $veriler->result(); 
	}
	
	function listele4($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("blogkategori"); 
		return $veriler->result(); 
	}
	
	function toplamblog(){
		$result = $this->db->select("*")
		->from("blog")
		->get()
		->result();
		return count($result);
	}
	
	function kategoriesle1($id){
	$result = $this->db->select("*")
	->from("blogkategori")
	->where("id",$id)
	->get()
	->result();
	return $result;
}

  function kategoriblog(){
	  $result = $this->db->select("*")
	  ->from("blogkategori")
	  ->get()
	  ->result();
	  return $result;
  }
  
  function blog($data){
	  $result = $this->db->insert("blog",$data);
	  return $result;
  }
  
  function blogyazicek($id){

	  $result = $this->db->select("*")
	  ->from("blog")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function blogyaziduzenleme($id,$data){
	  $this->db->where("id",$id);
	  $result = $this->db->update("blog",$data);
	  return $result;
  }
  
  function blogyazisil($id){
	  $result = $this->db->delete("blog",array("id" => $id));
	  return $result;
  }
  
  function blogresimcek($id){
	  $result = $this->db->select("*")
	  ->from("blog")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function blogkategoriekle($data){
	  $result = $this->db->insert("blogkategori",$data);
	  return $result;
  }
  
  function blogkategorisil($id){
	  $result = $this->db->delete("blogkategori",array("id" => $id));
	  return $result;
  }
  
  function blogkategoriduzenle($id,$data){
	  $this->db->where("id",$id);
	  $result = $this->db->update("blogkategori",array("kategori_adi" => $data , "seflink" => permalink($data)));
	  return $result;
  }
  
  function blogkategoricek($id){

	  $result = $this->db->select("*")
	  ->from("blogkategori")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function blogkategori(){
	  $result = $this->db->select("*")
	  ->from("blogkategori")
	  ->get()
	  ->result();
	  return $result;
  }
  
  function blogyaziresimcek($id){
	  $result = $this->db->select("*")
	  ->from("blog")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }

   
  function blogyazicekk(){
	  $result = $this->db->query("SELECT * FROM blog LIMIT 0,3");
	  return $result->result();
  }
  
  function hakkimizdacekek(){
	  $result = $this->db->select("*")
	  ->from("hakkimizda")
	  ->where("id",1)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function iletisimcekek(){
	  $result = $this->db->select("*")
	  ->from("iletisim")
	  ->where("id",1)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function listele5($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("blog"); 
		return $veriler->result(); 
    }
	
  function yazicekgetir($seflink){
	  $result = $this->db->select("*")
	  ->from("blog")
	  ->where("blog_seflink",$seflink)
	  ->get()
	  ->row();
	  return $result;
  } 
  
  function listele6($limit, $start,$id)   
	{
		
		$this->db->where("kategori",$id);
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("blog"); 
		return $veriler->result(); 
    }
	
	function kategoriyegoretoplam($seflink){
		$result = $this->db->select("*")
		->from("blogkategori")
		->get()
		->result();
		return count($result);
	}
	
	
	function listele7($limit,$start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("urunler"); 
		return $veriler->result(); 
    }
	
	function urunresimleri($data2){
		$this->db->insert("urunresimleri",$data2);
		$img_id = $this->db->insert_id();
		return $img_id;
	}
	
	function urunp($id){
		$result = $this->db->select("*")
		->from("urunresimleri")
		->where("urun_id",$id)
		->get()
		->row();
		return $result;
	}
	
	function urunresmguncelle($id,$data){
		$this->db->where("urun_id",$id);
		$result = $this->db->update("urunresimleri",$data);
		return $result;
	}
	
	function urunresimekle($data){
		$result = $this->db->insert("urunresimleri",$data);
		return $result;
	}
	
	function urunresmicekeceksilcek($al){
		$result = $this->db->select("*")
		->from("urunresimleri")
		->where("urun_id",$al)
		->get()
		->row();
		return $result;
	}
	
	function menuyekategoricek(){
		$result = $this->db->select("*")
		->from("kategoriler")
		->get()
		->result();
		return $result;
	}
	
	function menuyekle($data){
		$result = $this->db->insert("menuayarlari",$data);
		return $result;
	}
	
	function toplammenusayisi(){
		$result = $this->db->select("*")
		->from("menuayarlari")
		->get()
		->result();
		return count($result);
	}
	
	function listele10($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("menuayarlari"); 
		return $veriler->result(); 
	}
	
	
	function menusil($id){
		$result = $this->db->delete("menuayarlari",array("id" => $id));
		return $result;
	}
	
	function menubilgiduzenle($id){
		$result = $this->db->select("*")
		->from("menuayarlari")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	
	function tumkategorim(){
		$result = $this->db->select("*")
		->from("kategoriler")
		->get()
		->result();
		return $result;
	}
	
	
	
	function menuduzenle($id,$data){
		$this->db->where("id",$id);
		$result = $this->db->update("menuayarlari",$data);
		return $result;
	}
	
	function toplamreferanslar(){
		$result = $this->db->select("*")
		->from("referanslar")
		->get()
		->result();
		return count($result);
	}
	
	function listele15($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("referanslar");
		
		return $veriler->result(); 
	}
	
	function referansekle($data){
		
		$result = $this->db->insert("referanslar",$data);
		return $result;
	}
	
	function referansil($id){
		$result = $this->db->delete("referanslar",array("id" => $id));
		return $result;
	}
	
	function referansgetir($id){
		$result = $this->db->select("*")
		->from("referanslar")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	
	function referansresim($id){
	$result = $this->db->select("referans_resmi")
	->from("referanslar")
	->where("id",$id)
	->get()
	->row();
	return $result;
    }
	
	function referansupdate($id,$data){
		$this->db->where("id",$id);
		$result = $this->db->update("referanslar",$data);
		return $result;
	}
	
	function listele16($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("haberbulteni");
		
		return $veriler->result(); 
	}
	
	function toplamabone(){
		$result = $this->db->select("*")
		->from("haberbulteni")
		->get()
		->result();
		return count($result);
	}
	
	function haberbulteniekle($data){
		$result = $this->db->insert("haberbulteni",$data);
		return $result;
	}
	
	function abonesil($id){
		$result = $this->db->delete("haberbulteni",array("id" => $id));
		return $result;
	}
	
	function tumaboneler(){
		$result = $this->db->select("*")
		->from("haberbulteni")
		->get()
		->result();
		return $result;
	}
	
	
   
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
  
  

  

}



?>