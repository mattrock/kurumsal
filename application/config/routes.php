<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'anasayfa';
$route['hakkimizda'] = 'anasayfa/hakkimizda';
$route['iletisim'] = 'anasayfa/iletisim';
$route['hizmetler'] = 'anasayfa/hizmetler';
$route['hizmetler/(:any)'] = 'anasayfa/hizmetler/$1';
$route['hizmet/(:any)'] =  'anasayfa/yazi/$1';
$route['kategori/(:any)'] =  'anasayfa/kategori/$1';
$route['galeri'] = 'anasayfa/galeri';
$route['galeri/(:any)'] =  'anasayfa/galeri/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
