<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class yonetim extends CI_Controller {

	public function index()
	{
		$this->load->view('yonetim/giris');
		
	}
	
	public function guvenlik(){
	
		$this->form_validation->set_rules("kuladi","Kullanıcı adı","required|trim");
		$this->form_validation->set_rules("sifre","Şifre","required|trim");
		$this->form_validation->set_message("required",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>%s Boş Olamaz !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
		
		if($this->form_validation->run()){
			$kuladi = $this->input->post("kuladi");
			$sifre = $this->input->post("sifre");
			
			$this->load->model("sql");
			$result = $this->sql->admin($kuladi,sha1($sifre));
			
			if($result){
				
				$this->session->set_userdata("kontrol",TRUE);
				$this->session->set_userdata("uyebilgi",$result);				
				
				redirect("yonetimpaneli");
				
			}
			else{
				$this->session->set_flashdata("hata",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kullanıcı adı veya Şifre yanlış.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetim');
			}
			
		}
		else{
			$this->load->view("yonetim/giris");
		}
		
		
	}
}
