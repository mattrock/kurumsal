<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class anasayfa extends CI_Controller {

	public function index()
	{
		$this->load->model("sql");
		$data["siteayarlari"] = $this->sql->siteayarlari();
		$data["sliderlist"] = $this->sql->sliderlist();
		$data["blogkategori"] = $this->sql->blogkategori();
		$data["blogyazi"] = $this->sql->blogyazicekk();
		$data["hakkimizdacek"] = $this->sql->hakkimizdacekek();
		$data["iletisimcekek"] = $this->sql->iletisimcekek();
		$this->load->view('index',$data);
	}
	
	public function hakkimizda(){
		$this->load->model("sql");
		$data["siteayarlari"] = $this->sql->siteayarlari();
		$data["sliderlist"] = $this->sql->sliderlist();
		$data["blogkategori"] = $this->sql->blogkategori();
		$data["blogyazi"] = $this->sql->blogyazicekk();
		$data["hakkimizdacek"] = $this->sql->hakkimizdacekek();
		$data["iletisimcekek"] = $this->sql->iletisimcekek();
		
		$this->load->view("includes/header",$data);
		
		$this->load->view("hakkimizda",$data);
		
		$this->load->view("includes/footer",$data);
	}
	
	public function iletisim(){
		$this->load->model("sql");
		$data["siteayarlari"] = $this->sql->siteayarlari();
		$data["sliderlist"] = $this->sql->sliderlist();
		$data["blogkategori"] = $this->sql->blogkategori();
		$data["blogyazi"] = $this->sql->blogyazicekk();
		$data["hakkimizdacek"] = $this->sql->hakkimizdacekek();
		$data["iletisimcekek"] = $this->sql->iletisimcekek();
		
		$data["iletisimcek"] = $this->sql->iletisimcek();
		
		$this->load->view("includes/header",$data);
		
		$this->load->view("iletisim",$data);
		
		$this->load->view("includes/footer",$data);
	}
	
	public function hizmetler(){
		
		$this->load->model("sql");
		$data["siteayarlari"] = $this->sql->siteayarlari();
		$data["sliderlist"] = $this->sql->sliderlist();
		$data["blogkategori"] = $this->sql->blogkategori();
		$data["blogyazi"] = $this->sql->blogyazicekk();
		$data["hakkimizdacek"] = $this->sql->hakkimizdacekek();
		$data["iletisimcekek"] = $this->sql->iletisimcekek();
		

		$config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 0;  
		$config['per_page'] =2;   
		$config['total_rows'] =$this->sql->toplamyazi();  
		$config['base_url'] = site_url()."/hizmetler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele5($config['per_page'], $offset);  
		
		
		$this->load->view("includes/header",$data);
		
		$this->load->view("hizmetler",$data);
		
		$this->load->view("includes/footer",$data);
	}
	
	
	public function yazi($seflink){

		$this->load->model("sql");
		
		$data["yazilar"] = $this->sql->yazicekgetir($seflink);
		$data["siteayarlari"] = $this->sql->siteayarlari();
		$data["sliderlist"] = $this->sql->sliderlist();
		$data["blogkategori"] = $this->sql->blogkategori();
		$data["blogyazi"] = $this->sql->blogyazicekk();
		$data["hakkimizdacek"] = $this->sql->hakkimizdacekek();
		$data["iletisimcekek"] = $this->sql->iletisimcekek();
		
		$this->load->view("includes/header",$data);
		
		$this->load->view("yazi",$data);
		
		$this->load->view("includes/footer",$data);
		
	}
	
	public function kategori($seflink){
		
		$this->load->model("sql");
		
		
		
		
		
		$data["siteayarlari"] = $this->sql->siteayarlari();
		$data["sliderlist"] = $this->sql->sliderlist();
		$data["blogkategori"] = $this->sql->blogkategori();
		$data["blogyazi"] = $this->sql->blogyazicekk();
		$data["hakkimizdacek"] = $this->sql->hakkimizdacekek();
		$data["iletisimcekek"] = $this->sql->iletisimcekek();
		

		$config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 1;  
		$config['per_page'] =6;   
		$config['total_rows'] = $this->sql->kategoriyegoretoplam($seflink);  
		$config['base_url'] = site_url()."/kategori"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele6($config['per_page'], $offset,$seflink);
		
		$this->load->view("includes/header",$data);
		
		$this->load->view("kategori",$data);
		
		$this->load->view("includes/footer",$data);
		
	}
	
	
	
	public function galeri(){
		$this->load->model("sql");
		
		$data["siteayarlari"] = $this->sql->siteayarlari();
		$data["sliderlist"] = $this->sql->sliderlist();
		$data["blogkategori"] = $this->sql->blogkategori();
		$data["blogyazi"] = $this->sql->blogyazicekk();
		$data["hakkimizdacek"] = $this->sql->hakkimizdacekek();
		$data["iletisimcekek"] = $this->sql->iletisimcekek();
		
		$config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 0;  
		$config['per_page'] =6;   
		$config['total_rows'] = $this->sql->toplamurunler();  
		$config['base_url'] = site_url()."/galeri"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele7($config['per_page'], $offset);
		
		
		$this->load->view("includes/header",$data);
		
		$this->load->view("galeri",$data);
		
		$this->load->view("includes/footer",$data);
		
	}
	
	
	
	
	
	
	
	

	
	
	
	
}
