<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class yonetimpaneli extends CI_Controller {
	
	public function __construct(){
		
		parent:: __construct();{
			$this->security();
			$this->load->helper("security");
		}
		
	}
	
	
	function  security(){
		$kontrol = $this->session->userdata("kontrol");
		
		if(!isset($kontrol) || $kontrol != 1){
			redirect("yonetim/giris");
		}
		
	}
	

	public function index()
	{
		$this->load->model("sql");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->sql->toplamurunler();  
		$config['base_url'] = site_url()."yonetimpaneli/index"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
		
		

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele($config['per_page'], $offset);  
		$data["urunler"] = $this->sql->urunler();
		$data["toplamurunler"] = $this->sql->toplamurunler();
		$data["slider"] = $this->sql->toplamslider();
		$data["kategoritop"] = $this->sql->toplamkategori();
		$data["toplamyazi"] = $this->sql->toplamyazi();
		
		$data["sliderozet"] = $this->sql->sliderozet();
		$this->load->view('yonetim/index',$data);
	}
	
	public function siteayarlari(){
		$this->load->model("sql");
		$motor = $this->sql->faviconer();
		$eskifav = $motor->site_favicon;
		$eskilogo = $motor->site_logo;
		
		if($_POST){
			    
				
				$dosyayolu = 'uploads/';
				$dosya1 = $_FILES["file"];
				$dosya2 = $_FILES["file1"];
				
				if($dosya1["name"] == "" and $dosya2["name"] == ""){
					
					
					
			    $siteadi = $this->input->post('siteadi',TRUE);                       
				$siteaciklamasi = $this->input->post('siteaciklamasi',TRUE);                       
				$anahtarkelimeler = $this->input->post('anahtarkelimeler',TRUE);                       
				$siteyazar = $this->input->post('siteyazar',TRUE);                       
				                       
                
				$data = array(
				"site_logo" => $eskilogo,
				"site_favicon" => $eskifav,
				"site_adi" => $siteadi,
				"site_anahtar" => $anahtarkelimeler,
				"yazar" => $siteyazar,
				"site_aciklamasi" => $siteaciklamasi
				);
                
				
				
				$result = $this->sql->siteayarekle($data);
               
			   if($result){
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Güncelleme Başarılı !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
			   }
					
					
				}
				else if($dosya1["name"] != "" and $dosya2["name"] == ""){
					
					  $maxboyut = 5000000;
					  if($dosya1["size"] > $maxboyut){
						  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Logunun boyutu 5MB den büyük olmamalıdır.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
					  }
					  else{

						  if($dosya1["type"] == "image/png" || $dosya1["type"] == "image/jpeg" || $dosya1["type"] == "image/jpg"){
							  
							  $date = time();
							  $dosyauzantisi = explode(".",$dosya1["name"]);
							 
							  $dosyadi = rand(0,9999999).$date.".".$dosyauzantisi[1];
							  
							  $tasi = move_uploaded_file($dosya1["tmp_name"],$dosyayolu.$dosyadi);
							  
						  if($tasi){
							  
							  $siteadi = $this->input->post('siteadi',TRUE);                       
				              $siteaciklamasi = $this->input->post('siteaciklamasi',TRUE);                       
				              $anahtarkelimeler = $this->input->post('anahtarkelimeler',TRUE);                       
				              $siteyazar = $this->input->post('siteyazar',TRUE);                       
				                       
                
				$data = array(
				"site_logo" => $dosyadi,
				"site_favicon" => $eskifav,
				"site_adi" => $siteadi,
				"site_anahtar" => $anahtarkelimeler,
				"yazar" => $siteyazar,
				"site_aciklamasi" => $siteaciklamasi
				);
                
				
				
				$result = $this->sql->siteayarekle($data);
               
			   if($result){
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Güncelleme Başarılı !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
			   }
							  
						  }
							  
							  
						  }
						  else{
							 $this->session->set_flashdata("basarili",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Dosya tipi PNG,JPG,JPEG olmalıdır</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari'); 
						  }
						  
						 
						  
						  
						  
					  }
					  
				}
				else if ($dosya1["name"] == "" and $dosya2["name"] != ""){
					
					  $maxboyut = 5000000;
					  if($dosya2["size"] > $maxboyut){
						  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Favicon boyutu 5MB den büyük olmamalıdır.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
					  }
					  else{

						  if($dosya2["type"] == "image/png" || $dosya2["type"] == "image/jpeg" || $dosya2["type"] == "image/jpg"){
							  
							  $date = time();
							  $dosyauzantisi = explode(".",$dosya2["name"]);
							 
							  $dosyadi = rand(0,9999999).$date.".".$dosyauzantisi[1];
							  
							  $tasi = move_uploaded_file($dosya2["tmp_name"],$dosyayolu.$dosyadi);
							  
						  if($tasi){
							  $siteadi = $this->input->post('siteadi',TRUE);                       
				$siteaciklamasi = $this->input->post('siteaciklamasi',TRUE);                       
				$anahtarkelimeler = $this->input->post('anahtarkelimeler',TRUE);                       
				$siteyazar = $this->input->post('siteyazar',TRUE);                       
				                       
                
				$data = array(
				"site_logo" => $eskilogo,
				"site_favicon" => $dosyadi,
				"site_adi" => $siteadi,
				"site_anahtar" => $anahtarkelimeler,
				"yazar" => $siteyazar,
				"site_aciklamasi" => $siteaciklamasi
				);
                
				
				
				$result = $this->sql->siteayarekle($data);
               
			   if($result){
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Güncelleme Başarılı !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
			   }
						  }
							  
							  
						  }
						  else{
							  
							  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Dosya tipi PNG,JPG,JPEG olmalıdır."</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
							  
							 
						  }
						  
						 
						  
						  
						  
					  }
					  
					
					
				}
				else{
					$maxboyut = 50000;
					  if($dosya1["size"] > $maxboyut and $dosya2["size"] ){
						  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Dosya Boyutu 5MB büyük olmamalıdır.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
					  }
					  else{

						  if($dosya1["type"] != "image/png" || $dosya1["type"] != "image/jpeg" || $dosya1["type"] != "image/jpg" or $dosya2["type"] != "image/png" || $dosya2["type"] != "image/jpeg" || $dosya2["type"] != "image/jpg")
						  {

							  
							  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Dosya tipi PNG,JPG,JPEG olmalıdır</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
							  
						  }
						  
						  
						  
						  else{
							 
							 $date = time();
							  
							  $dosyauzantisi1 = explode(".",$dosya1["name"]);
							  $dosyadi1 = rand(0,9999999).$date.".".$dosyauzantisi1[1];
							  
							  $dosyauzantisi2 = explode(".",$dosya2["name"]);
							  $dosyadi2 = rand(0,9999999).$date.".".$dosyauzantisi2[1];
							  
							  $tasi1 = move_uploaded_file($dosya1["tmp_name"],$dosyayolu.$dosyadi1);
							  $tasi = move_uploaded_file($dosya2["tmp_name"],$dosyayolu.$dosyadi2);
							  
						  if($tasi and $tasi1){
							  $siteadi = $this->input->post('siteadi',TRUE);                       
				$siteaciklamasi = $this->input->post('siteaciklamasi',TRUE);                       
				$anahtarkelimeler = $this->input->post('anahtarkelimeler',TRUE);                       
				$siteyazar = $this->input->post('siteyazar',TRUE);                       
				                       
                
				$data = array(
				"site_logo" => $dosyadi1,
				"site_favicon" => $dosyadi2,
				"site_adi" => $siteadi,
				"site_anahtar" => $anahtarkelimeler,
				"yazar" => $siteyazar,
				"site_aciklamasi" => $siteaciklamasi
				);
                
				
				
				$result = $this->sql->siteayarekle($data);
               
			   if($result){
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Güncelleme Başarılı !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/siteayarlari');
			   }
						  }
							 
							 
						  }
						  
						 
						  
						  
						  
					  }
					  
					  
					  
					
					
				} 
				
				
				
				
				
				
				    
                
			
		}
		
		
		
		
		$data["siteayarlari"] = $this->sql->siteayarcek();
		
		
		$this->load->view("yonetim/site_ayarlari",$data);
}


   public function slider(){
	   
	   $this->load->model("sql");
	   
	    $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->sql->toplamslider();  
		$config['base_url'] = site_url()."yonetimpaneli/slider"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele2($config['per_page'], $offset);  
		
		
		$data["sliderlar"] = $this->sql->slidercek();
	   
	   $this->load->view("yonetim/slider",$data);
   
   
   }
   
   public function urunler(){
	   
	   $this->load->model("sql");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->sql->toplamurunler();  
		$config['base_url'] = site_url()."yonetimpaneli/urunler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele($config['per_page'], $offset);  
		
	
		
		
	   
	   $this->load->view("yonetim/urunler",$data);
   }
   
   public function hakkimizda(){
	   
	   $this->load->model("sql");
	   
	   if($_POST){
		   $baslik = $this->input->post("baslik");
		   $icerik = $this->input->post("editor_content");
		   $icerik1 = htmlspecialchars($icerik, ENT_QUOTES);
		   $result =  $this->sql->hakkimizdaup($icerik1,$baslik);
		  
		  if($result){
			  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Güncelleme Başarılı !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/hakkimizda');
		  }
		  
	   }
	   
	   
	   $data["hakkimizda"] = $this->sql->hakkimizdacek();
	   $this->load->view("yonetim/hakkimizda",$data);
       
   }
   
   
    public function iletisim(){
		$this->load->model("sql");
		
		if($_POST){
			
			$fb = $this->input->post("fb");
			$tw = $this->input->post("tw");
			$google = $this->input->post("google");
			$adres = $this->input->post("adres");
			$mail = $this->input->post("mail");
			$tel = $this->input->post("tel");
			$googlemaps = $this->input->post("googlemaps");
			$baslik = $this->input->post("baslik1");
			
			
			$data = array(
			"fb_adresi" =>  $fb,
			"tw_adresi" => $tw,
			"g_adresi" =>  $google,
			"adres" => $adres,
			"mail" => $mail,
			"tel" => $tel,
			"g_maps" =>  htmlspecialchars($googlemaps, ENT_QUOTES),
			"seflink" => permalink($baslik),
			"baslik" => $baslik
			);
			
			
			$result = $this->sql->iletisimup($data);
			
			if($result){
				$this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Güncelleme Başarılı !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/iletisim');
			}
		}
		
		$data["iletisim"] = $this->sql->iletisimcek();
	   $this->load->view("yonetim/iletisim",$data);
   }
   
   public function sliderekle(){
	   $this->load->model("sql");
	   if($_POST){
			    $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
				if(!$this->upload->do_upload('file')){
					$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/sliderekle/');
				}
                
				$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				$resim = $data["upload_data"]["file_name"];
		
				
				$slideradi = $this->input->post('sliderbaslik');                       
				$sliderslogan = $this->input->post('sliderslogan');                       
				                   
				                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"slider_adi" => $slideradi,
				"slider_resmi" =>  $resim,
				"slider_slogan" =>  $sliderslogan,
				"slider_tarih" => $date,
				);
                
				
				
				$result = $this->sql->sliderekle($data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Slider Eklendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/sliderekle');
			   }   
			
		}
	   
	   $this->load->view("yonetim/slider_ekle");
   }
   
   
   public function urunekle(){
	   $this->load->model("sql");

	  
	  $data["kategoriler"] = $this->sql->kategorilist();
	   if($_POST){
		   
			    
				
				$urunadi = $this->input->post("urunadi");
				$kategoriadi = $this->input->post("kategori");
				$aciklamasi = $this->input->post("aciklamasi");
				
				$path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png|avi|mp4|mp3|wmv|pdf';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);
				$finished = $this->upload->do_upload('file');
				
				if (!$finished){
					
					$error = array('error' => $this->upload->display_errors());
					
					if($error["error"] == "<p>You did not select a file to upload.</p>"){
					
					$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				$resim = $data["upload_data"]["file_name"];
		        
				
				                 
                
				$date = date("d.m.Y");
				
				$data = array(
				"urun_adi" => $urunadi,
				"urun_tarih" =>  $date,
				"urun_kategori" =>  $kategoriadi,
				"seflink" => permalink($urunadi),
				"urun_aciklamasi" => $aciklamasi
				);
				
				$result = $this->sql->urunekle($data);
				
				$data2 = array(
				"urunresmi" => $resim,
				"urun_id" => $result,
				);
                
				$this->sql->urunresimekle($data2);
				
				
               
			   if($data){
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Ürün Eklendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/urunekle/');
			   }
			   
					}
					
					else {
						$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');

                        redirect('yonetimpaneli/urunekle');
					}
					
                        
                }
				
				else{
				
                
				
				$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				$resim = $data["upload_data"]["file_name"];
		        
				
				                 
                
				$date = date("d.m.Y");
				
				$data = array(
				"urun_adi" => $urunadi,
				"urun_tarih" =>  $date,
				"urun_kategori" =>  $kategoriadi,
				"seflink" => permalink($urunadi),
				"urun_aciklamasi" => $aciklamasi
				);
				
				$result = $this->sql->urunekle($data);
				
				$data2 = array(
				"urunresmi" => $resim,
				"urun_id" => $result,
				);
                
				$this->sql->urunresimekle($data2);
				
				
               
			   if($data){
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Ürün Eklendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/urunekle/');
			   }
			  			   
			
				}
			
		}
	   
	   
	   
	   $this->load->view("yonetim/urun_ekle",$data);
   }
   
   public function kategoriler(){
	  $this->load->model("sql");
	  
	  
	    $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] = $this->sql->toplamkategori();  
		$config['base_url'] = site_url()."yonetimpaneli/kategoriler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele1($config['per_page'], $offset);  
	  
	  
	 
	  if($_POST){
		 $kategoriadi =  $this->input->post("kategoriadi");
		 
		 $data = array(
		 "kategori_adi" => $kategoriadi,
		 "seflink" => permalink($kategoriadi)
		 );
		 
		 $result = $this->sql->kategoriekle($data);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'
			 <div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Kategori Eklendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/kategoriler');
		 }
		 
	  }
	  
	  
	   
	   $data["kategoriler"] =  $this->sql->kategorilist();
	   $this->load->view("yonetim/kategoriler",$data);
   }
   
   public function menuayarlari(){
	    
		$this->load->model("sql");
	   
	    $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] = $this->sql->toplammenusayisi();  
		$config['base_url'] = site_url()."yonetimpaneli/menuayarlari"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele10($config['per_page'], $offset); 
		
	   
	   
	   
	   
	   
	   $this->load->view("yonetim/menuler",$data);
	   
	   
	   
   }
   
   public function menuekle(){
	   $this->load->model("sql");
	   
	  $data["kategoriler"] =  $this->sql->menuyekategoricek();
	  
	  if($_POST){
		  
		  $menuadi = $this->input->post("menuadi");
		  $kategori = $this->input->post("kategori");
		  
		  $parcala = implode(",",$kategori);
		  

		  
		  $date = date("d.m.Y");
		  
		  

		  $data = array(
		  "menu_adi" => $menuadi,
		  "menu_eklenme_tarihi" => $date,
		  "menu_alt_kategori_id" => $parcala,
		  "seflink" => permalink($menuadi)
		  );
		  
		  $result = $this->sql->menuyekle($data);
		  
		  
		  
		  
		  
		  if($result){
			  
			  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Menü Eklendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/menuekle');
			  
		  }
		  
	  }
	   
	   $this->load->view("yonetim/menu_ekle",$data);
   }
   
   public function urunduzenle($all){
        $this->load->model("sql");
		
		
		
	    $data['uruncek'] = $this->sql->uruncek($all);
        $eskiresim = $this->sql->urunp($all);
        $eskiresim = $eskiresim->urunresmi;

	   if($_POST){
		   
		   $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
				
				$this->upload->initialize($config);
				
				$finished = $this->upload->do_upload('file');
				
				
				if(!$finished){
					
					
					$error = array('error' => $this->upload->display_errors());
					
					if($error["error"] == "<p>You did not select a file to upload.</p>"){
						
					
					$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$urunadi = $this->input->post('urunadi');                       
				$kategori = $this->input->post('kategori');                       
				$aciklamasi = $this->input->post('aciklamasi');                       
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"urun_adi" => $urunadi,
				"urun_tarih" =>  $date,
				"urun_aciklamasi" => $aciklamasi,
				"urun_kategori" => $kategori,
				"seflink" => permalink($urunadi)
				);
				
				$data2 = array(
				"urunresmi" => $resim
				);
                
				$this->sql->urunresmguncelle($all,$data2);
				
				$result = $this->sql->urunupdate($all,$data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Ürün Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/urunduzenle/'.$all);
			   }
					
					
					}
					else{
						$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/urunduzenle/'.$all);
						
					}
					
					
				}
				
				
				else {
				
				$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$urunadi = $this->input->post('urunadi');                       
				$kategori = $this->input->post('kategori');                       
				$aciklamasi = $this->input->post('aciklamasi');                       
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"urun_adi" => $urunadi,
				"urun_tarih" =>  $date,
				"urun_aciklamasi" => $aciklamasi,
				"urun_kategori" => $kategori,
				"seflink" => permalink($urunadi)
				);
				
				$data2 = array(
				"urunresmi" => $resim
				);
                
				$this->sql->urunresmguncelle($all,$data2);
				
				$result = $this->sql->urunupdate($all,$data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Ürün Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/urunduzenle/'.$all);
			   }

			   
				}
		   
	   }
	   
	   $data["kategoriler"] =  $this->sql->kategorilist();
	   $this->load->view("yonetim/urun_duzenle",$data);
   }
   
   public function sliderduzenle($al){
	   
	   $this->load->model("sql");
	   $data["slider"] = $this->sql->sliderguncelle($al);
	   $data["esk"] = $this->sql->slideraresim($al);
       $eskiresim =  $data["esk"]->slider_resmi;
	   
	   
	   if($_POST){
		   
		   $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
				$finished = $this->upload->do_upload('file');
				
				if(!$finished){
					
					$error = array('error' => $this->upload->display_errors());
					
					if($error["error"] == "<p>You did not select a file to upload.</p>"){
						
						$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$slideradi = $this->input->post('sld_baslik');                       
				$sliderslogan = $this->input->post('sld_duzenle');                       
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"slider_adi" => $slideradi,
				"slider_resmi" =>  $resim,
				"slider_slogan" =>  $sliderslogan,
				"slider_tarih" => $date,
				);
                
				
				
				$result = $this->sql->sliderupdate($al,$data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Slider Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/sliderduzenle/'.$al);
			   }   
						
					}
					else{
						$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/sliderduzenle/'.$al);
					}
					
					
				}
				
				else{
				
				
				
				$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$slideradi = $this->input->post('sld_baslik');                       
				$sliderslogan = $this->input->post('sld_duzenle');                       
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"slider_adi" => $slideradi,
				"slider_resmi" =>  $resim,
				"slider_slogan" =>  $sliderslogan,
				"slider_tarih" => $date,
				);
                
				
				
				$result = $this->sql->sliderupdate($al,$data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Slider Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/sliderduzenle/'.$al);
			   }   
	   }
		   
	   }
	   
	        
	   $this->load->view("yonetim/slider_duzenle",$data);
	   
	   
   }
   
   public function slidersil($al){
	   $this->load->model("sql");
	   $slidercek = $this->sql->slidercektek($al);
	 
	   
	   $result =  $this->sql->slidersil($al);
	   
	   $dizin = "uploads/";
	   
	   
	   $resimsil = unlink( $dizin . $slidercek->slider_resmi);
	   
	  
	   
	   if($result){
		   $this->session->set_flashdata("silindi",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Slider Silindi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/slider');
	   }
   }
   
   public function urunsil($al){
	   
	   $this->load->model("sql");
	   
	   $urunsil =   $this->sql->urunsil($al);
	   
       $urunresmicek = $this->sql->urunresmicekeceksilcek($al);
	   $urunresmim = $urunresmicek->urunresmi;
	   
	   $dizin = "uploads/";
	   
	   
	   $resimsil = unlink( $dizin . $urunresmim);
	   
	   
	 
	 
	   if($urunsil){
		   $this->session->set_flashdata("silindi",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Ürün Silindi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/urunler');
	   }
   }
   
   public function kategoriduzenle($id){
	   $this->load->model("sql");
	   
	   if($_POST){
		   
		   $kategoriadi = $this->input->post("kategoriadi");
		   
		   $data = array(
		   "kategori_adi" => $kategoriadi,
		   "seflink" => permalink($kategoriadi)
		   );
		   
		   $result = $this->sql->kategoriduzenle($id,$data);
		   
		   if($result){
			   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Kategori Güncellendi!</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
		   }
		   
	   }
	   
	   $data["kategori"] = $this->sql->kategoricek($id);
	   $this->load->view("yonetim/kategori_duzenle",$data);
   }
   
   public function kategorisil($alde){
	   $this->load->model("sql");
	 $result = $this->sql->kategorisil($alde);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Ürün Silindi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/kategoriler');
	   }
   
   }
   public function cikis(){
	   $this->session->sess_destroy();
	   $this->session->set_flashdata("degisir",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Sistemden Çıkış Yapıldı.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
	   $this->load->view("yonetim/giris");
   }
   
   public function exit(){
	   $this->session->sess_destroy();
	    $this->session->set_flashdata("degisir",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Bilgileriniz değişti giriş yapın.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
	   $this->load->view("yonetim/giris");
   }
   
   
  public function profil(){
	  
	  $this->load->model("sql");
	  $data["profil"] = $this->sql->profilcek();
	  $eskiresim = $this->sql->profilresimcek();
	  $eskiresim = $eskiresim->resim;
	 
	  

      echo $this->pagination->create_links();
	  
	  if($_POST){
		   
		        $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
				
				
				
				
				
				$finished = $this->upload->do_upload('file1');
				
				if(!$finished){

                $error = array('error' => $this->upload->display_errors());

				
				if($error["error"] == "<p>You did not select a file to upload.</p>"){
					
			    $data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$adsoyad = $this->input->post('yadi');                       
				$kullaniciadi = $this->input->post('ykuladi');                       
				$sifre = $this->input->post('ysifre');                       
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"kuladi" => $kullaniciadi,
				"sifre" =>  sha1($sifre),
				"adsoyad" =>  $adsoyad,
				"resim" => $resim,
				);
                
				
				
				$result = $this->sql->profilupdate($data);
               
			   if($result){
				   
				   $uye = $this->session->userdata("uyebilgi");
				   
				   $uye->kuladi = $kullaniciadi;
				   $uye->sifre =  $sifre;
				   $uye->adsoyad = $adsoyad;
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Yönetim Bilgileri Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/exit');
					
			   }
					
				}
				
				else{			
				
				$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/profil'); 
				}
					
				}
				else{
					
				$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$adsoyad = $this->input->post('yadi');                       
				$kullaniciadi = $this->input->post('ykuladi');                       
				$sifre = $this->input->post('ysifre');                       
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"kuladi" => $kullaniciadi,
				"sifre" =>  sha1($sifre),
				"adsoyad" =>  $adsoyad,
				"resim" => $resim,
				);
                
				
				
				$result = $this->sql->profilupdate($data);
               
			   if($result){
				   
				   $uye = $this->session->userdata("uyebilgi");
				   
				   $uye->kuladi = $kullaniciadi;
				   $uye->sifre =  $sifre;
				   $uye->adsoyad = $adsoyad;
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Yönetim Bilgileri Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/exit');
					
			   }
					
				}
				
				   
	  
		   
	   }
	  
	  
	   $this->load->view("yonetim/profil",$data);
	   
	  
  }
  
  function blog(){

	  $this->load->model("sql");

	    $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] = $this->sql->toplamblog();  
		$config['base_url'] = site_url()."yonetimpaneli/blog"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele3($config['per_page'], $offset);  
	  
	  
	  $this->load->view("yonetim/blog",$data);
  }
  
  function yaziekle(){
	  $this->load->model("sql");
	  $data["kategoriler"] =  $this->sql->kategoriblog();

	 if($_POST){
		 
		 
		        $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
				if(!$this->upload->do_upload('file')){

				$this->session->set_flashdata("basarili",'<div id="card-alert" class="card red" style="display:block; background:red !important;">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/yaziekle');
				}
                
				$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];
		 
		 
		 $yazibaslik = $this->input->post("yazibaslik");
		 $yaziicerik = $this->input->post("editor_content");
		 $yazikategori = $this->input->post("kategori");
		 $tags = $this->input->post("tags");
		 
		 $date = date("d:m:Y");
		 
		 $data = array(
		 "blog_baslik" => htmlspecialchars($yazibaslik, ENT_QUOTES),
		 "blog_icerik" => htmlspecialchars($yaziicerik, ENT_QUOTES),
		 "blog_etiket" => htmlspecialchars($tags, ENT_QUOTES),
		 "blog_seflink" => permalink($yazibaslik),
		 "kategori" => $yazikategori,
		 "tarih" => $date,
		 "resim" => $resim,
		 );
		 
		$result =  $this->sql->blog($data);
		
		if($result){
			$this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Yazı Eklendi!</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/yaziekle');
		}
		 
	 }
	 
	  
	  $this->load->view("yonetim/yaziekle",$data);
  }
  
  public function blogyaziduzenle($id){
	  $this->load->model("sql");
	  $data["kategoriler"] =  $this->sql->kategoriblog();
	  $data["blogyazi"] = $this->sql->blogyazicek($id);
	  $eskresim = $this->sql->blogyaziresimcek($id);
	  $eskresim = $eskresim->resim;
	  
	  if($_POST){
		  
		        $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				$finished = $this->upload->do_upload('file');
				
				if(!$finished){
					
					$error = array('error' => $this->upload->display_errors());
					
					if($error["error"] == "<p>You did not select a file to upload.</p>"){
					
					$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];
				
				if($resim == null){
					 $resim = $eskresim;
				} 
		  
		  
		 $yazibaslik = $this->input->post("yazibaslik");
		 $editor_content =  $this->input->post("editor_content");
		 $kategori =  $this->input->post("kategori");
		 $tags =  $this->input->post("tags");
		 
		 $date = date("d:m:Y");
		 
		 $data = array(
		 "blog_baslik" => $yazibaslik,
		 "blog_icerik" => $editor_content,
		 "blog_etiket" => $tags,
		 "blog_seflink" => permalink($yazibaslik),
		 "kategori" => $kategori,
		 "tarih" => $date,
		 "resim" => $resim,
		 );
		 
		 $result = $this->sql->blogyaziduzenleme($id,$data);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Yazı Düzenlendi!</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/blogyaziduzenle/'.$id);
		 }
					
					}
					else{
						
					$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red" style="display:block; background:red !important;">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/blogyaziduzenle/'.$id);
						
					}
					
					
					
				}
                
				
				
				$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];
				
				if($resim == null){
					 $resim = $eskresim;
				} 
		  
		  
		 $yazibaslik = $this->input->post("yazibaslik");
		 $editor_content =  $this->input->post("editor_content");
		 $kategori =  $this->input->post("kategori");
		 $tags =  $this->input->post("tags");
		 
		 $date = date("d:m:Y");
		 
		 $data = array(
		 "blog_baslik" => $yazibaslik,
		 "blog_icerik" => $editor_content,
		 "blog_etiket" => $tags,
		 "blog_seflink" => permalink($yazibaslik),
		 "kategori" => $kategori,
		 "tarih" => $date,
		 "resim" => $resim,
		 );
		 
		 $result = $this->sql->blogyaziduzenleme($id,$data);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Yazı Düzenlendi!</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/blogyaziduzenle/'.$id);
		 }
		  
	  }
	  
	  $this->load->view("yonetim/yaziduzenle",$data);
	  
  }
  
  
  public function blogyazisil($id){
	  $this->load->model("sql");

	
	 $blgresimcek = $this->sql->blogresimcek($id);
	 $resim = $blgresimcek->resim;
	 
	 $dizin = "uploads/";
	 
	 $resimsil = unlink($dizin.$resim);
	 
	 
	  $result =  $this->sql->blogyazisil($id);

	  if($result){
		  $this->session->set_flashdata("silindi",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Yazı Silindi</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/blog');
	  }
	  
	  
  }
  
  function blogkategori(){
	  $this->load->model("sql");
	  
	  
	    $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] = $this->sql->toplamyazi();  
		$config['base_url'] = site_url()."yonetimpaneli/kategoriler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele4($config['per_page'], $offset);  
	  
	  
	 
	  if($_POST){
		 $kategoriadi =  $this->input->post("kategoriadi");
		 
		 $data = array(
		 "kategori_adi" => $kategoriadi,
		 "seflink" => permalink($kategoriadi)
		 );
		 
		 $result = $this->sql->blogkategoriekle($data);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'
			 <div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Kategori Eklendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/blogkategori');
		 }
		 
	  }

	   $data["kategoriler"] =  $this->sql->kategoriblog();

	  $this->load->view("yonetim/blogkategori",$data);
  }
  
  public function blogkategorisilme($id){
	  $this->load->model("sql");
	  $result = $this->sql->blogkategorisil($id);
	  
	  if($result){
		  $this->session->set_flashdata("silindi",'
			 <div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Kategori Silindi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/blogkategori');
	  }
	  
  }
  
  public function blogkategoriduzenleme($id){
	  
	  $this->load->model("sql");
	  
	  $data["kategori"] = $this->sql->blogkategoricek($id);
	  
	  
	  if($_POST){
		  
		  $kategori_adi = $this->input->post("kategoriadi");
		  
		  $result = $this->sql->blogkategoriduzenle($id,$kategori_adi);
		  
		  if($result){
		  $this->session->set_flashdata("basarili",'
			 <div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Kategori Düzenlendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/blogkategoriduzenleme/'.$id);
	  }
		  
	  }
	  
	  
	  $this->load->view("yonetim/blogkategoriduzenle",$data);
  }
  
  
  
  public function menusil($id){
	 
	 $this->load->model("sql");
	 $result = $this->sql->menusil($id);
	 
	 if($result){
		 $this->session->set_flashdata("basarili",'
			 <div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Menü Silindi.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
				redirect('yonetimpaneli/menuayarlari');
	 }
	  
  }
  
  public function menuduzenle($id){
	  $this->load->model("sql");
	  
	  
	  
	  $data["menucek"] = $this->sql->menubilgiduzenle($id);
	  
	  $data["tumkategori"] = $this->sql->tumkategorim();
	  
	  if($_POST){
		  
		  $menuadi = $this->input->post("menuadi",1);
		  $kategoriler = $this->input->post("kategori",1);
		  
		  $parcala = implode(",",$kategoriler);
		  
		  $date = date("d.m.Y");
		  
		  $data = array(
		  "menu_adi" => $menuadi,
		  "menu_eklenme_tarihi" => $date,
		  "menu_alt_kategori_id" => $parcala,
		  "seflink" => permalink($menuadi)
		  );
		 
		 $result =  $this->sql->menuduzenle($id,$data);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'
			 <div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Menü Düzenlendi.</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
				redirect('yonetimpaneli/menuduzenle/'.$id);
		 }
		  
	  }
	  
	  $this->load->view("yonetim/menu_duzenle",$data);
  }
  
  public function referanslar(){
	  $this->load->model("sql");
	  
	    $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] = $this->sql->toplamreferanslar();  
		$config['base_url'] = site_url()."yonetimpaneli/referanslar"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->sql->listele15($config['per_page'], $offset); 
	  
	  
	  $this->load->view("yonetim/referanslar",$data);
	  
  }
  
  public function referansekle(){
	  $this->load->model("sql");
	  
	  if($_POST){
		  
		        $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
		        
				if(!$this->upload->do_upload('file')){
					$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/referansekle/');
				}
                
				$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				$resim = $data["upload_data"]["file_name"];
		
				
				$referansadi = $this->input->post('referansadi');                                             
				                   
				                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"referans_adi" => strip_tags($referansadi),
				"referans_resmi" =>  $resim,
				"referans_tarihi" =>  $date,
				);
                
				
				
				$result = $this->sql->referansekle($data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Referans Eklendi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/referansekle');
			   }
		  
	  }
	  
	  $this->load->view("yonetim/referans_ekle");
  }
  
  public function referansil($id){
	   $this->load->model("sql");
	   $result = $this->sql->referansil($id);
	   
	   if($result){
		   
		   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Referans Silindi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/referanslar');
		   
	   }
	   
  }
  
  public function referansduzenle($al){
	  $this->load->model("sql");
	  $data["referanslar"] = $this->sql->referansgetir($al);
	  
	   $data["esk"] = $this->sql->referansresim($al);
       $eskiresim =  $data["esk"]->referans_resmi;
	   
	   
	   if($_POST){
		   
		        $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
				$finished = $this->upload->do_upload('file');
				
				if(!$finished){
					
					$error = array('error' => $this->upload->display_errors());
					
					if($error["error"] == "<p>You did not select a file to upload.</p>"){
						
						$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$referansadi = $this->input->post('referansadi');                       
				                      
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"referans_adi" => $referansadi,
				"referans_resmi" =>  $resim,
				"referans_tarihi" => $date,
				);
                
				
				
				$result = $this->sql->referansupdate($al,$data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Referans Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/referansduzenle/'.$al);
			   }   
						
					}
					else{
						$this->session->set_flashdata("basarisiz",'<div id="card-alert" class="card red">
                      <div class="card-content white-text">
                        <p>Kabul edilen türler PNG,JPG,GİF</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/referansduzenle/'.$al);
					}
					
					
				}
				
				else{
				
				
				
				$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];	
				
		
				
				$referansadi = $this->input->post('referansadi',1);                        
				                   
				if($resim == null){
					 $resim = $eskiresim;
				}                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"referans_adi" => strip_tags($referansadi),
				"referans_resmi" =>  $resim,
				"referans_tarihi" => $date,
				);
                
				
				
				$result = $this->sql->referansupdate($al,$data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Referans Güncellendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/referansduzenle/'.$al);
			   }   
	   }
		   
	   }
	  
	  
	  $this->load->view("yonetim/referans_duzenle",$data);
  }
  
  public function haberbulteni(){
	  
	  $this->load->model("sql");
	  
	  
	    $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] = $this->sql->toplamabone();  
		$config['base_url'] = site_url()."yonetimpaneli/haberbulteni"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	   
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->listele16($config['per_page'], $offset);
	  
	  
	  $this->load->view("yonetim/haberbulteni",$data);
  }
  
  public function haberbulteniekle(){
	  $this->load->model("sql");
	  
	  if($_POST){
		  
		  $abonemail = $this->input->post("abonemail",1);
		  
		  $date = date("d.m.Y");
		  
		  $data = array(
		  "mail_adresi" => strip_tags($abonemail),
		  "tarih" => $date,
		  );
		  
		  $result = $this->sql->haberbulteniekle($data);
		  
		  if($result){

			  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Abone Eklendi. !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/haberbulteni/');
			  
		  }
		  
	  }
	  
	  $this->load->view("yonetim/haberbulteni_ekle");
  }
  
  public function abonesil($id){
	  $this->load->model("sql");
	  $result = $this->sql->abonesil($id);
	  
	  if($result){
		  
		  $this->session->set_flashdata("basarili",'<div id="card-alert" class="card green">
                      <div class="card-content white-text">
                        <p>Abone Silindi !</p>
                      </div>
                      <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>');
					redirect('yonetimpaneli/haberbulteni/');
		  
	  }
	  
  }
  
  
  public function excelaktar(){
     
    $this->load->model("sql");	 
    
   
	
   
	
	function exportExcel($filename='ExportExcel',$columns=array(),$data=array(),$replaceDotCol=array()){
    header('Content-Encoding: UTF-8');
    header('Content-Type: text/plain; charset=utf-8'); 
    header("Content-disposition: attachment; filename=".$filename.".xls");
    echo "\xEF\xBB\xBF"; 
      
    $say=count($columns);
	
      
    echo '<table border="1"><tr>';
    foreach($columns as $v){
        echo '<th style="background-color:#FFA500">'.trim($v).'</th>';
    }
    echo '</tr>';
  
    foreach($data as $val){
        echo '<tr>';
        for($i=0; $i < $say; $i++){
  
            if(in_array($i,$replaceDotCol)){
                echo '<td>'.str_replace('.',',',@$val[$i]).'</td>';
            }else{
                echo '<td>'.$val[$i].'</td>';
            }
        }
        echo '</tr>';
    }
}


 
$columns=array();
 
 

$replaceDotCol=array(2); 
 

$columns=array(
    'Abone İd',
    'Abone Mail',
);
 

$data1 = $this->sql->tumaboneler();

$toplam = count($data1);


for($j=0; $j < $toplam; $j++){
	
$data[] =  array(
$data1[$j]->id,
$data1[$j]->mail_adresi,
);
	
} 




exportExcel('Abone',$columns,$data,$replaceDotCol);
	  
	  
  }
  
  
  public function metinaktar(){
	header('Content-Encoding: UTF-8');
    header('Content-Type: text/plain; charset=utf-8'); 
    header("Content-disposition: attachment; filename=Abonelistesi.txt");
    echo "\xEF\xBB\xBF";
	  
	  $this->load->model("sql");
	  $data = $this->sql->tumaboneler();
	  
	  foreach($data as $yaz){
		  echo $yaz->mail_adresi ."\r\n";
	  }
	  
	  
  }
  
  
  

}